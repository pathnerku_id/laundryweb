-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 19, 2023 at 12:07 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_laundry`
--

-- --------------------------------------------------------

--
-- Table structure for table `jenis_layanan`
--

CREATE TABLE `jenis_layanan` (
  `id_jenis_layanan` int(11) NOT NULL,
  `kode_jenis_layanan` char(10) NOT NULL,
  `id_layanan` int(11) NOT NULL,
  `nama_jenis_layanan` varchar(100) NOT NULL,
  `estimasi_waktu` int(11) NOT NULL,
  `tarif` mediumint(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `jenis_layanan`
--

INSERT INTO `jenis_layanan` (`id_jenis_layanan`, `kode_jenis_layanan`, `id_layanan`, `nama_jenis_layanan`, `estimasi_waktu`, `tarif`) VALUES
(85, 'J0001', 81, 'Reguler', 3, 5000),
(86, 'J0082', 81, 'Express', 1, 8000),
(90, 'J0082', 79, 'Reguler', 3, 5000),
(91, 'J0082', 79, 'Express', 1, 8000),
(92, 'J0082', 80, 'Reguler', 3, 6000),
(93, 'J0082', 80, 'Express', 1, 9000),
(99, 'J0083', 82, 'Express', 1, 7000),
(100, 'J0083', 82, 'Reguler', 3, 4000);

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id` int(11) NOT NULL,
  `nama_kecamatan` varchar(200) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id`, `nama_kecamatan`, `harga`) VALUES
(1, 'Banjarnegara', 2000),
(2, 'Madukara', 4000),
(3, 'Sigalu', 6000),
(4, 'Wanadadi', 8000);

-- --------------------------------------------------------

--
-- Table structure for table `layanan`
--

CREATE TABLE `layanan` (
  `id_layanan` int(11) NOT NULL,
  `kode_layanan` char(10) NOT NULL,
  `nama_layanan` varchar(50) NOT NULL,
  `gambar_layanan` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `layanan`
--

INSERT INTO `layanan` (`id_layanan`, `kode_layanan`, `nama_layanan`, `gambar_layanan`) VALUES
(79, 'P0001', 'Cuci Komplit', 'cuci komplit.png'),
(80, 'P0080', 'Cuci Satuan', 'cuci satuan.png'),
(81, 'P0081', 'Hanya Cuci', 'hanya cuci.png'),
(82, 'P0082', 'Cuci Kering', 'cuci kering.png');

-- --------------------------------------------------------

--
-- Table structure for table `log_aktivitas`
--

CREATE TABLE `log_aktivitas` (
  `id_aktivitas` int(11) NOT NULL,
  `waktu` datetime NOT NULL,
  `aktivitas` varchar(300) NOT NULL,
  `id_pengguna` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `log_aktivitas`
--

INSERT INTO `log_aktivitas` (`id_aktivitas`, `waktu`, `aktivitas`, `id_pengguna`) VALUES
(319, '2020-11-19 01:53:26', 'Edit Profil', 0),
(320, '2020-11-19 01:53:44', 'Logout', 16),
(321, '2020-11-19 01:53:49', 'Login', 16),
(322, '2020-11-19 01:54:20', 'Edit Profil', 0),
(323, '2020-11-19 01:55:42', 'Edit Profil', 16),
(324, '2020-11-19 02:13:37', 'Edit Profil', 16),
(325, '2020-11-19 02:13:41', 'Logout', 16),
(326, '2020-11-19 02:13:53', 'Login', 16),
(327, '2020-11-19 02:14:08', 'Edit Profil', 16),
(328, '2020-11-19 02:14:11', 'Logout', 16),
(329, '2020-11-19 02:14:17', 'Login', 16),
(330, '2020-11-19 02:14:33', 'Edit Profil', 16),
(331, '2020-11-19 02:14:36', 'Logout', 16),
(332, '2020-11-19 02:14:41', 'Login', 16),
(333, '2020-11-19 02:17:15', 'Edit Profil', 16),
(334, '2020-11-19 02:18:02', 'Edit Profil', 16),
(335, '2020-11-19 02:18:11', 'Edit Profil', 16),
(336, '2020-11-19 02:52:20', 'Edit Profil', 16),
(337, '2020-11-19 05:03:21', 'Logout', 16),
(338, '2020-11-19 05:03:41', 'Login', 16),
(343, '2020-11-19 07:02:09', 'Login', 16),
(346, '2020-11-19 07:23:50', 'Update Profil Aplikasi', 16),
(347, '2020-11-19 07:24:03', 'Update Profil Aplikasi', 16),
(348, '2020-11-19 07:24:06', 'Update Profil Aplikasi', 16),
(349, '2020-11-19 07:26:39', 'Update Profil Aplikasi', 16),
(350, '2020-11-19 07:26:48', 'Update Profil Aplikasi', 16),
(351, '2020-11-19 07:29:12', 'Update Profil Aplikasi', 16),
(352, '2020-11-19 07:36:10', 'Update Profil Aplikasi', 16),
(353, '2020-11-19 07:36:35', 'Edit Profil', 16),
(354, '2020-11-19 07:36:49', 'Edit Profil', 16),
(355, '2020-11-19 07:37:04', 'Tambah paket #P0083 ', 0),
(356, '2020-11-19 07:38:31', 'Update Profil Aplikasi', 16),
(357, '2020-11-19 07:56:08', 'Edit Profil', 16),
(358, '2020-11-19 07:57:06', 'Update Profil Aplikasi', 16),
(359, '2020-11-19 07:57:58', 'Update Profil Aplikasi', 16),
(360, '2020-11-19 07:58:45', 'Edit Profil', 16),
(361, '2020-11-19 07:58:53', 'Edit Profil', 16),
(362, '2020-11-19 07:59:00', 'Edit Profil', 16),
(363, '2020-11-19 08:03:26', 'Update Profil Aplikasi', 16),
(364, '2020-11-19 10:59:24', 'Tambah pengguna #U019 ', 0),
(365, '2020-11-19 11:00:03', 'Tambah pengguna #U022 ', 0),
(366, '2020-11-19 11:21:10', 'Tambah pengguna #U023 ', 0),
(367, '2020-11-19 11:21:38', 'Tambah pengguna #U023 ', 0),
(368, '2020-11-20 08:07:21', 'Login', 16),
(369, '2020-11-20 08:07:59', 'Tambah pengguna #U023 ', 0),
(370, '2020-11-20 08:33:34', 'Edit pengguna  #U022 ', 16),
(371, '2020-11-20 08:33:52', 'Edit pengguna  #U022 ', 16),
(372, '2020-11-20 08:33:59', 'Edit pengguna  #U022 ', 16),
(373, '2020-11-20 08:34:11', 'Edit pengguna  #U022 ', 16),
(374, '2020-11-20 08:35:20', 'Edit pengguna  #U022 ', 16),
(375, '2020-11-20 08:35:39', 'Edit pengguna  #U022 ', 16),
(376, '2020-11-20 08:35:58', 'Edit pengguna  #U014 ', 16),
(377, '2020-11-20 08:36:22', 'Edit pengguna  #U013 ', 16),
(378, '2020-11-20 08:37:20', 'Edit pengguna  #U014 ', 16),
(379, '2020-11-20 08:40:25', 'Edit pengguna  #U016 ', 16),
(380, '2020-11-20 08:41:57', 'Logout', 16),
(381, '2020-11-20 08:42:09', 'Login', 16),
(382, '2020-11-20 08:44:59', 'Logout', 16),
(383, '2020-11-20 08:45:05', 'Login', 16),
(384, '2020-11-20 08:45:19', 'Update Profil Aplikasi', 16),
(385, '2020-11-20 08:45:23', 'Logout', 16),
(386, '2020-11-20 08:45:58', 'Login', 16),
(388, '2020-11-20 09:05:38', 'Edit Profil', 16),
(389, '2020-11-20 09:05:57', 'Edit Profil', 16),
(390, '2020-11-20 09:06:40', 'Edit Profil', 16),
(391, '2020-11-20 09:06:56', 'Edit Profil', 16),
(393, '2020-11-20 09:09:50', 'Edit Profil', 16),
(394, '2020-11-20 09:11:05', 'Edit Profil', 16),
(395, '2020-11-20 09:12:57', 'Edit pengguna  #U016 ', 16),
(396, '2020-11-20 09:13:45', 'Edit pengguna  #U016 ', 16),
(397, '2020-11-20 09:14:19', 'Edit Profil', 16),
(398, '2020-11-20 09:15:04', 'Edit Profil', 16),
(399, '2020-11-20 09:15:51', 'Edit Profil', 16),
(400, '2020-11-20 09:16:01', 'Edit Profil', 16),
(401, '2020-11-20 09:16:22', 'Edit Profil', 16),
(402, '2020-11-20 09:16:34', 'Logout', 16),
(403, '2020-11-20 09:16:38', 'Login', 16),
(404, '2020-11-20 09:19:25', 'Edit Profil', 16),
(405, '2020-11-20 09:19:49', 'Edit Profil', 16),
(406, '2020-11-20 09:21:05', 'Edit Profil', 16),
(407, '2020-11-20 09:22:39', 'Edit Profil', 16),
(408, '2020-11-20 09:22:49', 'Edit Profil', 16),
(409, '2020-11-20 09:24:07', 'Logout', 16),
(410, '2020-11-20 09:24:11', 'Login', 16),
(411, '2020-11-20 09:24:18', 'Edit Profil', 16),
(412, '2020-11-20 09:24:39', 'Logout', 16),
(413, '2020-11-20 09:24:43', 'Login', 16),
(414, '2020-11-20 09:25:17', 'Edit Profil', 16),
(415, '2020-11-20 09:28:59', 'Edit Profil', 16),
(416, '2020-11-20 09:29:08', 'Edit Profil', 16),
(417, '2020-11-20 09:29:23', 'Edit Profil', 16),
(418, '2020-11-20 09:34:55', 'Edit Profil', 16),
(419, '2020-11-20 01:41:53', 'Update Profil Aplikasi', 16),
(420, '2020-11-20 01:42:09', 'Update Profil Aplikasi', 16),
(421, '2020-11-20 01:47:51', 'Update Profil Aplikasi', 16),
(422, '2020-11-20 01:48:41', 'Update Profil Aplikasi', 16),
(423, '2020-11-20 01:48:56', 'Update Profil Aplikasi', 16),
(424, '2020-11-20 01:51:49', 'Update Profil Aplikasi', 16),
(425, '2020-11-20 01:54:17', 'Update Profil Aplikasi', 16),
(426, '2020-11-20 01:54:55', 'Update Profil Aplikasi', 16),
(427, '2020-11-20 01:55:07', 'Update Profil Aplikasi', 16),
(428, '2020-11-20 01:56:42', 'Update Profil Aplikasi', 16),
(429, '2020-11-20 01:57:48', 'Update Profil Aplikasi', 16),
(430, '2020-11-20 02:07:45', 'Update Profil Aplikasi', 16),
(431, '2020-11-20 02:19:02', 'Update Profil Aplikasi', 16),
(432, '2020-11-20 02:19:30', 'Update Profil Aplikasi', 16),
(433, '2020-11-20 02:20:01', 'Update Profil Aplikasi', 16),
(434, '2020-11-20 02:21:53', 'Update Profil Aplikasi', 16),
(435, '2020-11-20 02:22:19', 'Update Profil Aplikasi', 16),
(436, '2020-11-20 02:23:05', 'Update Profil Aplikasi', 16),
(437, '2020-11-20 02:23:25', 'Update Profil Aplikasi', 16),
(438, '2020-11-20 02:25:28', 'Update Profil Aplikasi', 16),
(439, '2020-11-20 02:26:15', 'Update Profil Aplikasi', 16),
(440, '2020-11-20 02:28:12', 'Update Profil Aplikasi', 16),
(441, '2020-11-20 02:28:28', 'Update Profil Aplikasi', 16),
(442, '2020-11-20 02:32:30', 'Update Profil Aplikasi', 16),
(443, '2020-11-20 03:04:40', 'Edit Profil', 16),
(444, '2020-11-20 03:14:53', 'Edit pengguna  #U011 ', 16),
(445, '2020-11-20 03:15:08', 'Edit pengguna  #U011 ', 16),
(446, '2020-11-20 03:15:51', 'Edit pengguna  #U016 ', 16),
(447, '2020-11-20 03:16:36', 'Edit Profil', 16),
(448, '2020-11-20 03:16:48', 'Edit Profil', 16),
(449, '2020-11-20 03:17:06', 'Edit Profil', 16),
(450, '2020-11-20 03:21:18', 'Edit pengguna  #U014 ', 16),
(451, '2020-11-20 03:21:36', 'Edit pengguna  #U013 ', 16),
(452, '2020-11-20 03:22:07', 'Edit pengguna  #U014 ', 16),
(453, '2020-11-20 03:22:30', 'Edit pengguna  #U011 ', 16),
(454, '2020-11-20 04:42:27', 'Logout', 16),
(455, '2020-11-20 11:09:09', 'Login', 16),
(456, '2020-11-21 09:33:42', 'Logout', 13),
(457, '2020-11-21 09:33:52', 'Login', 16),
(458, '2020-11-21 09:34:35', 'Login', 16),
(459, '2020-11-21 09:36:13', 'Tambah pengguna #U017 ', 16),
(460, '2020-11-21 09:36:22', 'Edit pengguna  #U017 ', 16),
(461, '2020-11-21 09:36:35', 'Logout', 16),
(462, '2020-11-22 09:39:44', 'Login', 16),
(463, '2020-11-22 09:39:52', 'Logout', 16),
(464, '2020-11-22 10:54:53', 'Login', 16),
(465, '2020-11-22 01:05:45', 'Logout', 16),
(466, '2020-11-22 09:21:04', 'Login', 16),
(467, '2020-11-22 09:21:32', 'Edit Profil', 16),
(468, '2020-11-22 09:21:48', 'Logout', 16),
(469, '2020-11-22 09:55:50', 'Login', 16),
(470, '2020-11-22 10:05:21', 'Logout', 16),
(471, '2020-11-23 11:08:30', 'Login', 16),
(472, '2020-11-23 01:01:14', 'Tambah layanan #J0083 ', 0),
(473, '2020-11-23 01:01:28', 'Tambah layanan #P0083 ', 0),
(474, '2020-11-23 01:17:54', 'Logout', 16),
(475, '2020-11-26 01:40:15', 'Login', 16),
(476, '2020-11-26 02:01:27', 'Logout', 16),
(477, '2020-11-26 02:21:07', 'Login', 16),
(478, '2020-11-26 02:21:27', 'Logout', 16),
(479, '2020-11-27 07:54:03', 'Login', 16),
(480, '2020-11-27 07:54:13', 'Logout', 16),
(481, '2020-11-28 10:13:55', 'Login', 16),
(482, '2020-11-28 10:14:05', 'Logout', 16),
(483, '2020-12-12 12:10:22', 'Login', 15),
(484, '2020-12-12 12:10:34', 'Logout', 15),
(485, '2020-12-12 12:10:39', 'Login', 16),
(486, '2020-12-20 03:04:14', 'Login', 16),
(487, '2020-12-20 03:05:55', 'Tambah layanan #J0083 ', 0),
(488, '2020-12-21 11:56:10', 'Login', 16),
(489, '2020-12-21 12:03:54', 'Tambah layanan #J0083 ', 0),
(490, '2020-12-21 01:03:03', 'Edit Profil', 16),
(491, '2020-12-21 01:05:46', 'Tambah layanan #P0083 ', 0),
(492, '2020-12-21 01:06:00', 'Tambah layanan #P0083 ', 0),
(493, '2020-12-21 01:06:06', 'Tambah layanan #J0083 ', 0),
(494, '2020-12-22 03:33:46', 'Tambah pengguna #U017 ', 16),
(495, '2020-12-22 03:40:39', 'Edit pengguna  #U017 ', 16),
(496, '2020-12-22 03:41:43', 'Tambah pengguna #U028 ', 16),
(497, '2020-12-22 03:41:59', 'Edit pengguna  #U028 ', 16),
(498, '2020-12-23 02:45:33', 'Tambah layanan #P0083 ', 0),
(499, '2020-12-23 03:29:33', 'Edit pengguna  #U016 ', 16),
(500, '2020-12-23 03:30:29', 'Tambah pengguna #U017 ', 16),
(501, '2020-12-23 03:31:57', 'Tambah pengguna #U030 ', 16),
(502, '2020-12-23 03:32:10', 'Edit pengguna  #U017 ', 16),
(503, '2020-12-23 03:32:55', 'Edit pengguna  #U016 ', 16),
(504, '2020-12-23 03:33:05', 'Edit pengguna  #U017 ', 16),
(505, '2020-12-23 03:36:25', 'Edit pengguna  #U016 ', 16),
(506, '2020-12-23 03:36:47', 'Edit Profil', 16),
(507, '2020-12-23 03:37:03', 'Edit Profil', 16),
(508, '2020-12-23 03:37:16', 'Edit Profil', 16),
(509, '2020-12-23 03:37:32', 'Edit pengguna  #U016 ', 16),
(510, '2020-12-23 03:37:42', 'Edit pengguna  #U016 ', 16),
(511, '2020-12-23 04:46:08', 'Edit pengguna  #U016 ', 16),
(512, '2020-12-23 04:48:53', 'Edit Profil', 16),
(513, '2020-12-23 04:57:01', 'Edit pengguna  #U016 ', 16),
(514, '2020-12-23 04:58:39', 'Edit pengguna  #U016 ', 16),
(515, '2020-12-23 04:59:21', 'Edit pengguna  #U016 ', 16),
(516, '2020-12-23 04:59:29', 'Edit pengguna  #U016 ', 16),
(517, '2020-12-24 10:21:21', 'Edit Profil', 16),
(518, '2020-12-24 10:22:01', 'Update Profil Aplikasi', 16),
(519, '2020-12-24 10:22:11', 'Update Profil Aplikasi', 16),
(520, '2020-12-24 01:26:24', 'Logout', 16),
(521, '2020-12-24 01:26:34', 'Login', 16),
(522, '2020-12-24 01:27:13', 'Logout', 16),
(523, '2020-12-24 13:27:24', 'Login', 16),
(524, '2020-12-24 01:27:31', 'Logout', 16),
(525, '2020-12-24 13:27:48', 'Login', 16),
(526, '2020-12-24 17:56:11', 'Input transaksi no invoice #T0289', 16),
(527, '2020-12-24 17:59:09', 'Update transaksi no invoice #T0289', 16),
(528, '2020-12-24 17:59:46', 'Tambah layanan #P0083 ', 0),
(529, '2020-12-24 18:00:21', 'Tambah layanan #P0094 ', 16),
(530, '2020-12-24 18:03:31', 'Tambah layanan #P0083 ', 16),
(531, '2020-12-24 18:03:33', 'Hapus layanan ID #95 ', 16),
(532, '2020-12-24 18:04:37', 'Tambah Jenis Layanan #J0083 ', 16),
(533, '2020-12-24 18:06:51', 'Hapus Jenis Layanan # ', 16),
(534, '2020-12-24 18:07:14', 'Tambah Jenis Layanan #J0083 ', 16),
(535, '2020-12-24 18:07:17', 'Hapus Jenis Layanan ID #110 ', 16),
(536, '2020-12-24 18:08:51', 'Tambah Pelanggan #PN0019 ', 16),
(537, '2020-12-24 18:09:14', 'Edit Pelanggan #PN0019 ', 16),
(538, '2020-12-24 18:09:33', 'Hapus Pelanggan #PN0019 ', 16),
(539, '2020-12-24 18:10:14', 'Tambah pengguna #U031 ', 16),
(540, '2020-12-24 18:10:41', 'Edit pengguna  #U031 ', 16),
(541, '2020-12-24 18:14:01', 'Tambah pengguna #U032 ', 16),
(542, '2020-12-24 18:14:39', 'Tambah pengguna # ', 16),
(543, '2020-12-24 18:15:06', 'Hapus pengguna ID #16 ', 16),
(544, '2020-12-24 18:17:12', 'Hapus Transaksi ID #289', 16),
(545, '2020-12-24 06:17:44', 'Edit Profil', 16),
(546, '2020-12-24 18:29:40', 'Input transaksi no invoice #T0289', 16),
(547, '2020-12-24 18:35:15', 'Input transaksi no invoice #T0291', 16),
(548, '2020-12-24 18:35:36', 'Input transaksi no invoice #T0292', 16),
(549, '2020-12-24 18:46:27', 'Tambah Jenis Layanan #J0083 ', 16),
(550, '2020-12-24 18:46:37', 'Hapus Jenis Layanan ID #111 ', 16),
(551, '2020-12-24 18:57:48', 'Input transaksi no invoice #T0293', 16),
(552, '2020-12-24 07:05:41', 'Edit Profil', 16),
(553, '2020-12-24 19:06:00', 'Edit pengguna  #U017 ', 16),
(554, '2020-12-26 19:52:23', 'Login', 29),
(555, '2020-12-26 19:52:43', 'Input transaksi no invoice #T0294', 29),
(556, '2020-12-26 19:52:53', 'Logout', 29),
(557, '2020-12-26 19:53:02', 'Login', 16),
(558, '2020-12-26 19:53:14', 'Logout', 16),
(559, '2020-12-26 19:53:21', 'Login', 30),
(560, '2023-04-30 15:08:32', 'Login', 11),
(561, '2023-04-30 15:10:04', 'Logout', 11),
(562, '2023-04-30 15:10:09', 'Login', 16),
(563, '2023-04-30 17:05:56', 'Login', 16),
(564, '2023-05-22 01:17:00', 'Login', 16),
(565, '2023-05-22 01:20:45', 'Logout', 16),
(566, '2023-05-22 01:21:24', 'Login', 16),
(567, '2023-05-22 01:33:19', 'Tambah Pelanggan #PN0019 ', 16),
(568, '2023-05-22 01:37:55', 'Edit Pelanggan #PN0019 ', 16),
(569, '2023-05-25 16:22:36', 'Login', 16),
(570, '2023-05-25 16:23:38', 'Logout', 16),
(571, '2023-05-25 16:40:31', 'Login', 16),
(572, '2023-05-25 17:12:06', 'Logout', 16),
(573, '2023-05-25 17:13:07', 'Login', 16),
(574, '2023-05-25 17:15:07', 'Logout', 16),
(575, '2023-05-25 18:09:59', 'Login', 16),
(576, '2023-05-25 18:10:44', 'Logout', 16),
(577, '2023-05-25 18:14:30', 'Login', 16),
(578, '2023-05-29 11:34:56', 'Login', 16),
(579, '2023-05-30 13:30:56', 'Login', 16),
(580, '2023-05-30 14:33:52', 'Login', 16),
(581, '2023-05-30 21:36:50', 'Login', 16),
(582, '2023-05-31 16:14:41', 'Login', 16),
(583, '2023-05-31 16:20:31', 'Login', 16),
(584, '2023-06-05 05:32:35', 'Login', 16),
(585, '2023-06-06 16:33:52', 'Login', 16),
(586, '2023-06-07 20:39:09', 'Login', 16),
(587, '2023-06-10 19:06:11', 'Login', 16),
(588, '2023-06-10 19:17:09', 'Logout', 16),
(589, '2023-06-10 19:29:42', 'Login', 16),
(590, '2023-06-10 19:36:30', 'Login', 16),
(591, '2023-06-11 21:47:55', 'Login', 16),
(592, '2023-06-11 21:48:45', 'Input transaksi no invoice #T0305', 16),
(593, '2023-06-13 02:20:42', 'Login', 16),
(594, '2023-06-13 02:24:07', 'Input transaksi no invoice #T0306', 16),
(595, '2023-06-13 02:25:07', 'Update transaksi no invoice #T0306', 16),
(596, '2023-06-13 02:27:27', 'Input transaksi no invoice #T0307', 16),
(597, '2023-06-13 02:28:57', 'Input transaksi no invoice #T0308', 16),
(598, '2023-06-13 02:29:26', 'Update transaksi no invoice #T0308', 16),
(599, '2023-06-13 02:44:15', 'Input transaksi no invoice #T0309', 16),
(600, '2023-06-13 02:44:50', 'Input transaksi no invoice #T0310', 16),
(601, '2023-06-13 02:45:04', 'Input transaksi no invoice #T0320', 16),
(602, '2023-06-13 02:47:34', 'Input transaksi no invoice #T0321', 16),
(603, '2023-06-14 17:13:04', 'Login', 16),
(604, '2023-06-15 06:32:05', 'Login', 16),
(605, '2023-06-18 08:36:16', 'Login', 16),
(606, '2023-06-19 12:02:46', 'Login', 16);

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL,
  `kode_pelanggan` char(9) NOT NULL,
  `nama_pelanggan` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `no_telp` char(14) NOT NULL,
  `alamat_pelanggan` varchar(50) NOT NULL,
  `jenis_kelamin` int(11) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `foto` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `kode_pelanggan`, `nama_pelanggan`, `username`, `password`, `no_telp`, `alamat_pelanggan`, `jenis_kelamin`, `tanggal_lahir`, `foto`, `status`) VALUES
(7, 'P0007', 'Danuar Antasari', '', '', '08237484492', 'Jl Mengkubumi no 11', 1, '1993-08-28', '', 1),
(8, 'P0008', 'Mila Supriani', '', '', '082322233', 'Jl maya no 76', 2, '2000-02-22', '', 1),
(9, 'P0009', 'Wahyu Atmajaya', '', '', '087823293222', 'Jl batu no 98', 1, '1997-11-11', '', 1),
(10, 'P0010', 'Rizki Kurniawan', '', '', '08237484492', 'Jl mantrijeron no 67', 1, '1996-08-11', '', 1),
(11, 'P0011', 'Ilham Sanjaya', '', '', '087823293233', 'Jl sukuarjo no 89', 1, '1998-12-19', '', 1),
(12, 'PN001', 'Rini Mustika', '', '', '082322230343', 'Jl cendana no 78', 2, '1991-08-26', '', 1),
(17, 'PN0013', 'Ferrry Setiawan', '', '', '08923274242', 'Jl banguntapan no 22', 1, '1993-11-11', '', 1),
(18, 'PN0018', 'Kornelia Putri', '', '', '08292324444', 'Jl Madura no 45', 1, '1989-11-23', '', 1),
(24, 'PN0019', 'yuyun', 'anaswaraa', '123', '123123223', 'kota malang', 1, '2023-05-22', '', 1),
(25, 'PN0025', 'yuyun', 'admin', '123456', '12358392739', 'Sambong Permai', 1, '2023-05-31', '', 1),
(26, 'PN0026', 'randos', 'anaswaraw', '1231', '123', '123', 1, '2023-05-31', '', 1),
(29, 'PN0027', 'yuyun', 'wer', '123', '', 'Sambong Permai', 1, '2023-06-28', '', 1),
(30, 'P-28982', 'budi', 'budi12', '123', '264846', 'malang', 2, '2022-12-12', '64863ab86ad217.50244661.png', 1),
(46, 'PN0046', 'Budi Raharjo', 'randi12', '123', '123', 'jl Mangga no 12', 1, '2019-06-18', '648ebc4cbe0d50.59467600.jpg', 1),
(47, 'PN0047', 'Bagus Raja Kita', 'bagus12', '123', '085452433121', 'Jl Batu no 12', 1, '2007-06-18', '648ebdd517f268.03871118.jpg', 1),
(48, 'PN0048', 'rangga tarantula', 'rangga12', '123', '08654578964', 'Jl tunjungan no 54', 1, '2023-06-18', '648ebef405bb53.96908972.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id_pengguna` int(11) NOT NULL,
  `kode_pengguna` char(9) NOT NULL,
  `nama_pengguna` varchar(50) NOT NULL,
  `email` varchar(35) NOT NULL,
  `no_telp` char(14) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `level` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id_pengguna`, `kode_pengguna`, `nama_pengguna`, `email`, `no_telp`, `foto`, `username`, `password`, `level`, `status`) VALUES
(1, 'U001', '11', 'arimurti95.sd@gmail.com', '11', '', '11', '11', '0', 1),
(2, 'U002', '22', 'arimurti95@gmail.com', '222', '', '22', '22', '0', 1),
(3, 'U003', '232', 'arimurti95.sd@gmail.com', '23', '', '23', '23', '0', 0),
(4, 'U004', '111', 'arimurti95.sd@gmail.com', '232', '', '22', '22', '0', 1),
(11, 'U011', 'Marshel Kurniawan', 'marshel.kurniawan@gmail.com', '08924232322', '20180701_143107.jpg', 'arimurti', '1045c9cfc701cdbcfe0928cff1571261', 'Manejer', 1),
(13, 'U013', 'kasir', 'lutfi.taher@gmail.com', '23245', '20191225_161031.jpg', 'kasir', 'kasir', 'Kasir', 1),
(15, 'U014', 'Dimas', 'arimurti95.sd@gmail.com', '082322230343', '20191222_121533.jpg', 'dimas', '7d49e40f4b3d8f68c19406a58303f826', 'Kasir', 1),
(16, 'U016', 'Setiawan Dimas', 'super_admin@laundry.com', '082322230343', '1.jpg', 'super_admin_laundry', '827ccb0eea8a706c4c34a16891f84e7b', 'Super Admin', 1),
(29, 'U017', 'Cristina Ayu', 'admin_laundry@laundry.com', '08234543544', 'safitri ayu.png', 'admin_laundry', '827ccb0eea8a706c4c34a16891f84e7b', 'Admin', 1),
(30, 'U030', 'Manajer Laundry', 'manajer@laundry.com', '089283777484', 'marshel.png', 'manajer_laundry', '827ccb0eea8a706c4c34a16891f84e7b', 'Manajer', 1);

-- --------------------------------------------------------

--
-- Table structure for table `profil_aplikasi`
--

CREATE TABLE `profil_aplikasi` (
  `id` int(11) NOT NULL,
  `nama_aplikasi` varchar(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_telp` char(14) NOT NULL,
  `website` varchar(50) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `no_rek` varchar(200) NOT NULL,
  `nama_rek` varchar(200) NOT NULL,
  `bank` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `profil_aplikasi`
--

INSERT INTO `profil_aplikasi` (`id`, `nama_aplikasi`, `alamat`, `no_telp`, `website`, `logo`, `no_rek`, `nama_rek`, `bank`) VALUES
(0, 'Majesty Laundry', 'Jl Karangbendo Kulon No 45', '082322230345', 'www.majestylaundry.com', 'logo.png', '1390011020138', 'DWI RETNA MARTUTI', 'Mandiri');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `no_invoice` char(10) NOT NULL,
  `tanggal_transaksi` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_pengguna` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `nama_pelanggan` varchar(100) NOT NULL,
  `no_telp` char(14) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `id_layanan` int(11) NOT NULL,
  `id_jenis_layanan` int(11) NOT NULL,
  `berat` double NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `total_biaya` mediumint(9) NOT NULL,
  `status_bayar` int(11) NOT NULL,
  `status_pengambilan` int(11) NOT NULL,
  `metode_pengiriman` int(11) NOT NULL,
  `keterangan` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `no_invoice`, `tanggal_transaksi`, `id_pengguna`, `id_pelanggan`, `nama_pelanggan`, `no_telp`, `alamat`, `id_layanan`, `id_jenis_layanan`, `berat`, `tanggal_masuk`, `tanggal_selesai`, `total_biaya`, `status_bayar`, `status_pengambilan`, `metode_pengiriman`, `keterangan`) VALUES
(281, 'T0001', '2020-12-24 09:52:00', 16, 8, 'Mila Supriani', '082322233', 'Jl maya no 76', 80, 92, 23, '2020-12-24', '2020-12-27', 138000, 0, 0, 0, '-'),
(282, 'T0282', '2020-12-22 04:51:00', 16, 0, 'Ferrry Setiawan', '08923274242', 'Jl banguntapan no 22', 81, 86, 3, '2020-12-22', '2020-12-23', 24000, 1, 0, 0, ''),
(284, 'T0284', '2020-12-24 08:44:00', 16, 0, 'Danuar Antasari', '08237484492', 'Jl Mengkubumi no 11', 80, 93, 23, '2020-12-24', '2020-12-25', 207000, 1, 1, 0, '23'),
(285, 'T0285', '2020-12-25 10:16:00', 29, 0, 'Wahyu Atmajaya', '087823293222', 'Jl batu no 98', 81, 86, 23, '2020-12-25', '2020-12-26', 184000, 1, 0, 0, '-'),
(286, 'T0286', '2020-12-23 10:20:00', 29, 7, 'Danuar Antasari', '08237484492', 'Jl Mengkubumi no 11', 80, 92, 2, '2020-12-23', '2020-12-26', 12000, 0, 0, 0, ''),
(288, 'T0288', '2020-12-19 06:28:00', 16, 8, 'Mila Supriani', '082322233', 'Jl maya no 76', 81, 85, 23, '2020-12-19', '2020-12-22', 115000, 0, 0, 0, '23'),
(294, 'T0294', '2020-12-19 12:52:00', 29, 9, 'Wahyujaya', '087823293222', 'Jl batu no 98', 79, 90, 23, '2020-12-19', '2020-12-22', 115000, 0, 0, 0, '23'),
(300, 'T0299', '2023-06-06 11:01:00', 13, 24, 'yuyun', '12358392739', 'Malang', 81, 86, 2, '0000-00-00', '2023-06-07', 25000, 0, 1, 0, '-'),
(301, 'T0301', '2023-06-08 07:49:00', 13, 24, 'yuyun', '123', '123', 79, 91, 2, '2023-06-08', '2023-06-09', 22000, 0, 0, 0, '-'),
(329, 'T0329', '2023-06-17 02:43:00', 13, 46, 'Budi Raharjo', '123', 'jl.melati no 12', 80, 93, 5, '2023-06-17', '2023-11-18', 53000, 0, 0, 1, '-'),
(330, 'T0330', '2023-06-17 02:45:00', 13, 46, 'Budi Raharjo', '123', 'jl.melati no 12', 80, 93, 5, '2023-06-17', '2023-11-18', 53000, 1, 1, 1, '-'),
(331, 'T0331', '2023-06-17 05:41:00', 13, 46, 'Budi Raharjo', '123', 'jl.melati no 12', 79, 91, 5, '2023-06-17', '2023-11-18', 48000, 0, 0, 1, '-'),
(332, 'T0332', '2023-06-17 08:58:00', 13, 46, 'Budi Raharjo', '123', 'jl.melati no 12', 80, 93, 5, '2023-06-17', '2023-11-18', 53000, 0, 0, 1, '-'),
(333, 'T0333', '2023-06-18 03:22:00', 13, 48, 'rangga tarantula', '08654578964', 'Jl tunjungan no 54', 80, 93, 7, '2023-06-18', '2023-11-28', 71000, 0, 0, 1, '-');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jenis_layanan`
--
ALTER TABLE `jenis_layanan`
  ADD PRIMARY KEY (`id_jenis_layanan`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `layanan`
--
ALTER TABLE `layanan`
  ADD PRIMARY KEY (`id_layanan`),
  ADD UNIQUE KEY `kode_produk` (`kode_layanan`);

--
-- Indexes for table `log_aktivitas`
--
ALTER TABLE `log_aktivitas`
  ADD PRIMARY KEY (`id_aktivitas`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`),
  ADD UNIQUE KEY `kode_pelanggan` (`kode_pelanggan`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_pengguna`);

--
-- Indexes for table `profil_aplikasi`
--
ALTER TABLE `profil_aplikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD UNIQUE KEY `no_invoice` (`no_invoice`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jenis_layanan`
--
ALTER TABLE `jenis_layanan`
  MODIFY `id_jenis_layanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `layanan`
--
ALTER TABLE `layanan`
  MODIFY `id_layanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `log_aktivitas`
--
ALTER TABLE `log_aktivitas`
  MODIFY `id_aktivitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=607;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_pengguna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=334;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
