<?php
include '../config/database.php';

$data = json_decode(file_get_contents('php://input'), true);


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      function input($data) {
        return $data;
    }
   try {
    $query = mysqli_query($kon, "SELECT max(id_transaksi) as kodeTerbesar FROM transaksi");
    $row = mysqli_fetch_array($query);
    $id_transaksi = $row['kodeTerbesar'];
    $id_transaksi++;
    $huruf = "T";
    $kodetransaksi = $huruf . sprintf("%04s", $id_transaksi);
    
    $no_invoice=input($kodetransaksi);
    $id_pengguna=13;
    
    $tanggal_masuk = new DateTime();
    $tanggal_masuk = $tanggal_masuk->format('Y/m/d');
    $jam_sekarang=date("H:i");
    $tanggal_transaksi=date("Y-m-d H:i",strtotime($tanggal_masuk.$jam_sekarang));
    
    $keterangan="-";
    $status_bayar=0;
    $status_pengambilan=0;
    
    $id_pelanggan = $data['id_pelanggan'];
    $nama_pelanggan = $data['nama_pelanggan'];
    $no_telp = $data['no_telp'];
    $alamat = $data['alamat'];
    $id_layanan = $data['id_layanan'];
    $id_jenis_layanan = $data['id_jenis_layanan'];
    $berat = $data['berat'];
    $tanggal_selesai = $data['tanggal_selesai'];
    $total_biaya = $data['total_biaya'];
    $metode_pengiriman = $data['metode_pengiriman'];
    $result = mysqli_query($kon, "INSERT INTO  transaksi  
    VALUES (
        '',
        '$no_invoice',
        '$tanggal_transaksi',
        '$id_pengguna',
        '$id_pelanggan',
        '$nama_pelanggan',
        '$no_telp',
        '$alamat',
        '$id_layanan',
        '$id_jenis_layanan',
        '$berat',
        '$tanggal_masuk',
        '$tanggal_selesai',
        '$total_biaya',
        '$status_bayar',
        '$status_pengambilan',
        '$metode_pengiriman',
        '$keterangan'
    )");
    
  if ($result) {
      $affected_rows = mysqli_affected_rows($kon);
      if ($affected_rows > 0) {
        $response = array('status' => true,'message'=>"data berhasil di tambahkan");
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
      } else {
        $response = array('status' => false,'message'=>"data gagal di tambahkan");
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
      }
  } else {
      echo "Terjadi kesalahan dalam menjalankan perintah SQL.";
  }
    $response = array('status' => true);
    header('Content-Type: application/json');
    echo json_encode($response);
    exit;
} catch (error) {
    $response = array('status' => false, 'message' => 'Gagal menambahkan data');
     echo json_encode($response);
     exit;
   }    
  
  }
?>