<?php
include '../config/database.php';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = json_decode(file_get_contents('php://input'), true);
    // Menerima data yang dikirimkan melalui metode POST
   try {
    $id_pelanggan = $data['id'];
    // Melakukan sanitasi data (hindari SQL Injection!)
    // Query untuk memeriksa kecocokan data login
    $query = "SELECT * FROM pelanggan WHERE id_pelanggan = '$id_pelanggan'";
    $queryLayanan = "SELECT * FROM layanan";
    $queryRekening = "SELECT * FROM profil_aplikasi";
    $queryJenisLayanan = "SELECT * FROM jenis_layanan";
    $queryKecamatan = "SELECT * FROM kecamatan";
    $queryTransaksi = "SELECT transaksi.*, pengguna.*, layanan.*
    FROM transaksi
    JOIN pengguna ON transaksi.id_pengguna = pengguna.id_pengguna
    JOIN layanan ON transaksi.id_layanan = layanan.id_layanan
    WHERE transaksi.id_pelanggan = '$id_pelanggan' AND transaksi.status_pengambilan = 1";
    $resultKecamatan = mysqli_query($kon, $queryKecamatan);
    $resultRekening = mysqli_query($kon, $queryRekening);
    $resultTransaksi = mysqli_query($kon, $queryTransaksi);
    $resultLayanan = mysqli_query($kon, $queryLayanan); 
    $resultJenisLayanan = mysqli_query($kon, $queryJenisLayanan); 
    $result = mysqli_query($kon, $query);
    $row = mysqli_fetch_assoc($result);
    $rowRekening = mysqli_fetch_assoc($resultRekening);
    
    // Memeriksa keberhasilan login
    if (mysqli_num_rows($result) > 0) {
      // Jika login berhasil
      $response = array('status' => true, 'dataUser' => array(), 'dataLayanan' => array(),'dataTransaksi' => array(),'dataJenisLayanan'=>array(),'dataKecamatan'=>array(),'dataRekening'=>array());
      $response['dataUser'][] = $row;
      $response['dataRekening'][] = $rowRekening;
      while ( $rowLayanan = mysqli_fetch_assoc($resultLayanan)) {
        $response['dataLayanan'][] = $rowLayanan;
      }
      while ( $rowJenisLayanan = mysqli_fetch_assoc($resultJenisLayanan)) {
        $response['dataJenisLayanan'][] = $rowJenisLayanan;
      }
      while ( $rowKecamatan = mysqli_fetch_assoc($resultKecamatan)) {
        $response['dataKecamatan'][] = $rowKecamatan;
      }
      if(mysqli_num_rows($resultTransaksi) > 0){
        while ( $rowTransaksi = mysqli_fetch_assoc($resultTransaksi)) {
          $response['dataTransaksi'][] = $rowTransaksi;
        }
      }
      else{
        $response['dataTransaksi'] = false; 
      }
    } else {
      // Jika login gagal
      $response = array('status' => false, 'message' => 'JSON parse error');
    }
  
    // Mengirimkan respons dalam format JSON
    header('Content-Type: application/json');
    echo json_encode($response);
   } catch (error) {
    $response = array('status' => false, 'message' => 'Username atau password salah');
    header('Content-Type: application/json');
    echo json_encode($response);
   }
  }
 
    // Endpoint lainnya bisa ditambahkan di sini
?>