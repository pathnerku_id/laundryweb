<?php
include '../config/database.php';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = json_decode(file_get_contents('php://input'), true);
    // Menerima data yang dikirimkan melalui metode POST
   try {
    $username = $data['username'];
    $password = $data['password'];
    // Melakukan sanitasi data (hindari SQL Injection!)
    $username = mysqli_real_escape_string($kon, $username);
    $password = mysqli_real_escape_string($kon, $password);
  
    // Query untuk memeriksa kecocokan data login
    $query = "SELECT * FROM pelanggan WHERE username = '$username' AND password = '$password'";
    $result = mysqli_query($kon, $query);
    $resid = mysqli_fetch_assoc($result);
    // Memeriksa keberhasilan login
    if (mysqli_num_rows($result) > 0) {
      // Jika login berhasil
      $resid =  $resid['id_pelanggan'];
      $response = array('status' => true, 'message' => 'Login berhasil','id'=>$resid);
    } else {
      // Jika login gagal
      $response = array('status' => false, 'message' => 'Username atau password salah');
    }
  
    // Mengirimkan respons dalam format JSON
    header('Content-Type: application/json');
    echo json_encode($response);
   } catch (error) {
    $response = array('status' => false, 'message' => 'Username atau password salah');
    header('Content-Type: application/json');
    echo json_encode($response);
   }
  }
 
    // Endpoint lainnya bisa ditambahkan di sini
?>