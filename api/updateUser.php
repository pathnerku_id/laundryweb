<?php
try {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        include '../config/database.php';
        // Mengambil data dari permintaan POST
        
        
        $id_pelanggan = $_POST['id_pelanggan'];
        $kode_pelanggan = $_POST['kode_pelanggan'];
        $nama_pelanggan = $_POST['nama_pelanggan'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $no_telp = $_POST['no_telp'];
        $alamat_pelanggan = $_POST['alamat_pelanggan'];
        $jenis_kelamin = $_POST['jenis_kelamin'];
        $tanggal_lahir = $_POST['tanggal_lahir'];
        $change = $_POST['change'];
        
        $userLama = mysqli_query($kon,"SELECT * FROM pelanggan WHERE pelanggan.id_pelanggan = '$id_pelanggan'");
        $row = mysqli_fetch_assoc($userLama);
        
        // Memeriksa koneksi
        if ($kon->connect_error) {
            die("Koneksi gagal: " . $kon->connect_error);
        }
        
        // Mendapatkan informasi tentang file yang diunggah
        $uniqueName = $row['foto'];
        if($change==="true"){
            $fileExtension = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
            // Filter jenis file yang diizinkan
            $allowedExtensions = array('jpg', 'png');
            if (in_array($fileExtension, $allowedExtensions)) {
                $uniqueName = uniqid('', true) . '.' . $fileExtension;
                $targetDir = '../page/pelanggan/foto/';
                $targetFile = $targetDir . $uniqueName;
            }else{
                $response = array(
                    "status" => false,
                    "message" => "Format file tidak didukung. Hanya file JPG dan PNG yang diizinkan."
                );
            
                header('Content-Type: application/json');
                echo json_encode($response);
                exit;
            }
           
        }
        // Mengizinkan hanya format JPG dan PNG
    
        
        // Menyiapkan pernyataan SQL untuk memperbarui data
        $sql = "UPDATE pelanggan SET
                kode_pelanggan = '$kode_pelanggan',
                nama_pelanggan = '$nama_pelanggan',
                username = '$username',
                password = '$password',
                no_telp = '$no_telp',
                alamat_pelanggan = '$alamat_pelanggan',
                jenis_kelamin = '$jenis_kelamin',
                tanggal_lahir = '$tanggal_lahir',
                foto = '$uniqueName',
                status = '1'
                WHERE id_pelanggan = '$id_pelanggan'";
        
        // Menjalankan pernyataan SQL
        if ($kon->query($sql) === TRUE) {
            // Memindahkan file yang diunggah ke lokasi tujuan
            if($change==="true"){
                if (move_uploaded_file($_FILES["image"]["tmp_name"], $targetFile)) {
                    // Jika pembaruan berhasil dan file berhasil diunggah, berikan respons berhasil
                    unlink($targetDir . $row['foto']);
                    $response = array(
                        "status" => true,
                        "message" => "Data berhasil diperbarui dan file berhasil diunggah."
                    );
                    echo json_encode($response);
                    exit;
                } else {
                    // Jika pembaruan berhasil tetapi gagal mengunggah file, berikan respons dengan pesan kesalahan
                    $response = array(
                        "status" => false,
                        "message" => "Data berhasil diperbarui dan file berhasil diunggah."
                    );
                    echo json_encode($response);
                    exit;
                }
            }
            else {
                // Jika pembaruan berhasil tetapi gagal mengunggah file, berikan respons dengan pesan kesalahan
                $response = array(
                    "status" => true,
                    "message" => "Data berhasil diperbarui, tetapi gagal mengunggah file."
                );
                echo json_encode($response);
                exit;
            }
           
        } else {
            // Jika terjadi kesalahan dalam pembaruan, berikan respons kesalahan
            $response = array(
                "status" => false,
                "message" => "Gagal memperbarui data: " . $kon->error
            );
            echo json_encode($response);
            exit;
        }

}
}catch(error){
    $response = array('status' => false, 'message' => 'Gagal mengupdate data');
      header('Content-Type: application/json');
      echo json_encode($response);
      exit;
}

?>