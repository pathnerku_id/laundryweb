<?php
include '../config/database.php';
// Endpoint: /api.php?endpoint=uploadImage
try {
 if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  // Pastikan form memiliki atribut: enctype="multipart/form-data"
  
  // Menerima data yang dikirimkan melalui metode POST

  $uploadedFile = $_FILES['image'];
  
  // Memeriksa apakah ada file yang diunggah
  if ($uploadedFile['error'] === UPLOAD_ERR_OK) {
    // Mendapatkan informasi ekstensi file yang diunggah
    $fileExtension = strtolower(pathinfo($uploadedFile['name'], PATHINFO_EXTENSION));
  
    // Filter jenis file yang diizinkan
    $allowedExtensions = array('jpg', 'png');
  
    // Memeriksa apakah ekstensi file sesuai dengan filter
    if (in_array($fileExtension, $allowedExtensions)) {
      // Memeriksa dan memindahkan file ke direktori tujuan
      $uniqueName = uniqid('', true) . '.' . $fileExtension;
      
      $targetDir = '../page/pelanggan/foto/';
      $targetFile = $targetDir . $uniqueName;
        AddData($uniqueName,$uploadedFile['tmp_name'],$targetFile,$uploadedFile);
      // Memindahkan file yang diunggah ke direktori tujuan
      
    } else {
      // Jika jenis file tidak diizinkan
      $response = array('status' => false, 'message' => 'Jenis file tidak diizinkan');
    }
  } else {
    // Jika terjadi kesalahan saat mengunggah file
    $response = array('status' => false, 'message' => 'Terjadi kesalahan saat mengunggah file');
  }
  
  // Mengirimkan respons dalam format JSON
  header('Content-Type: application/json');
  echo json_encode($response);
  exit;
}
} catch (error) {
  $response = array('status' => false, 'message' => 'Gagal menambahkan data');
  echo json_encode($response);
  exit;
}

function AddData($img,$nameImg, $targetFile,$uploadedFile){
    global $kon;
    try {
          // Endpoint: /api.php?endpoint=addPelanggan
          // Mengambil data yang dikirimkan melalui metode POST
          $data = json_decode(file_get_contents('php://input'), true);
          $query = mysqli_query($kon, "SELECT max(id_pelanggan) as kodeTerbesar FROM pelanggan");
          $datas = mysqli_fetch_array($query);
          $id_pelanggan = $datas['kodeTerbesar'];
          $id_pelanggan++;
          $huruf = "PN";
          $kodepelanggan = $huruf . sprintf("%04s", $id_pelanggan);
          // Menyambungkan ke database MySQL
        
        
          // Memeriksa koneksi database
        
          // Menghindari SQL Injection dengan menggunakan prepared statement
          $query = "INSERT INTO pelanggan (kode_pelanggan, nama_pelanggan, username, password, no_telp, alamat_pelanggan, jenis_kelamin, tanggal_lahir, foto, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
          $status = 1;
          $stmt = mysqli_prepare($kon, $query);
          // Binding parameter pada prepared statement
          mysqli_stmt_bind_param(
            $stmt,
            'ssssssssss',
            $kodepelanggan,
            $_POST['nama_pelanggan'],
            $_POST['username'],
            $_POST['password'],
            $_POST['no_telp'],
            $_POST['alamat_pelanggan'],
            $_POST['jenis_kelamin'],
            $_POST['tanggal_lahir'],
            $img,
            $status
          );
        
          // Eksekusi prepared statement
          if (mysqli_stmt_execute($stmt)) {
            // Jika penambahan data berhasil
            if (move_uploaded_file($nameImg, $targetFile)) {
                // Jika pemindahan file berhasil
                $response = array('status' => true, 'message' => 'File berhasil diunggah');
              } else {
                // Jika pemindahan file gagal
                $response = array('status' => false, 'message' => 'Gagal memindahkan file');
              }
              $id_pelanggan = $datas['kodeTerbesar'];
              $id_pelanggan++;
            $response = array('status' => true, 'message' => 'Data berhasil ditambahkan','id'=>$id_pelanggan);
          } else {
            // Jika terjadi kesalahan saat menambahkan data
            $response = array('status' => false, 'message' => 'Gagal menambahkan data');
          }
        
          // Menutup prepared statement dan koneksi database
          mysqli_stmt_close($stmt);
          mysqli_close($kon);
        
          // Mengirimkan respons dalam format JSON
          header('Content-Type: application/json');
          echo json_encode($response);
          exit;
    } catch (error) {
      $response = array('status' => false, 'message' => 'Gagal menambahkan data');
      header('Content-Type: application/json');
      echo json_encode($response);
      exit;
    }
    }
    

?>