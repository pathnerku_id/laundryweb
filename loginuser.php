<?php
  session_start();
  if  (isset($_SESSION["id_pelanggan"])){
    header("Location:page/user");
  }
 
 include "config/database.php";
 $hasil=mysqli_query($kon,"select * from profil_aplikasi order by nama_aplikasi desc limit 1");
 $data = mysqli_fetch_array($hasil);
 $row = true;
 if (isset($_POST["login"])) {
    $username = $_POST["username"];
    $password = $_POST["password"];
    $result = mysqli_query($kon, "SELECT * FROM pelanggan WHERE pelanggan.username = '$username' AND pelanggan.password = '$password';");
    $row = mysqli_fetch_assoc($result);
    if($row !== NULL ){
        $_SESSION["id_pelanggan"] = $row["id_pelanggan"];
        $_SESSION["kode_pelanggan"] = $row["kode_pelanggan"];
        $_SESSION["username"] = $row["username"];
        $_SESSION["nama_pelanggan"] = $row["nama_pelanggan"];
    }
    if  (isset($_SESSION["id_pelanggan"])){
        header("Location:page/user");
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Aplikasi Selamanik Laundry</title>
    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="assets/font/font.css" rel="stylesheet">
    <link href="assets/css/user.css" rel="stylesheet">
</head>

<body>


    <div class="container">
        <div class="left">
            <div class="logo">
                <img src="page/aplikasi/logo/logo.png" width="100rem" class="imgs animation a7">
            </div>
            <div class="header">
                <h2 class="animation a1"><?php echo ucfirst($data['nama_aplikasi']);?></h2>
            </div>
            <form class="form" method="POST">
                <input type="text" name="username" class="form-field animation a3" placeholder="Username">
                <input type="password" name="password" class="form-field animation a4" placeholder="Password">
                <p class="animation a5"><a href="register.php">Register</a></p>
                <?php if($row == NULL){?>
                <p style="color:red;">Username & Password Salah!</p>
                <?php }?>
                <button class="animation a6 " style="cursor: pointer;" type="submit" name="login">LOGIN</button>
            </form>
        </div>
        <div class="right"></div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="assets/js/sb-admin-2.min.js"></script>

</body>

</html>