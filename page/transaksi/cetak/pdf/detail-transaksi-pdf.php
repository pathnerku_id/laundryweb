<?php
    //Mengambil plugin fpdf
    require('../../../../assets/vendor/fpdf/fpdf.php');
    $pdf = new FPDF('P', 'mm','Letter');

    //Membuat Koneksi ke database
    include '../../../../config/database.php';
    $query = mysqli_query($kon, "select * from profil_aplikasi order by nama_aplikasi desc limit 1");    
    $row = mysqli_fetch_array($query);

    $pdf->AddPage();
    $pdf->Image('../../../../page/aplikasi/logo/'.$row['logo'],15,5,30,30);
    $pdf->SetFont('Arial','B',21);
    $pdf->Cell(0,7,strtoupper($row['nama_aplikasi']),0,1,'C');
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(0,7,$row['alamat'].', Telp '.$row['no_telp'],0,1,'C');
    $pdf->Cell(0,7,$row['website'],0,1,'C');

    $no_invoice=$_GET['no_invoice'];
    $query = mysqli_query($kon, "SELECT * from transaksi t inner join layanan p on p.id_layanan=t.id_layanan inner join jenis_layanan j on j.id_jenis_layanan=t.id_jenis_layanan where t.no_invoice='$no_invoice'");    
    $data = mysqli_fetch_array($query);

    $no_invoice=$data['no_invoice'];

    $pdf->Cell(10,7,'',0,1);
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(50,6,'No Invoice',0,0);
    $pdf->Cell(20,6,': '.$data['no_invoice'],0,1);

    $pdf->Cell(50,6,'Tanggal Transaksi',0,0);
    $pdf->Cell(20,6,': '.date("d/m/Y",strtotime($data['tanggal_transaksi'])),0,1);

    $pdf->Cell(50,6,'Pelanggan',0,0);
    $pdf->Cell(20,6,': '.$data['nama_pelanggan'],0,1);

    $pdf->Cell(50,6,'No Telp',0,0);
    $pdf->Cell(20,6,': '.$data['no_telp'],0,1);

    if ($data['status_bayar']==1){
        $bayar='Telah dibayar';
    }else {
        $bayar='Belum dibayar';
    }
    $pdf->Cell(50,6,'Pembayaran',0,0);
    $pdf->Cell(20,6,': '.$bayar,0,1);

    
    if ($data['status_pengambilan']==1){
        $pengambilan='Telah diambil';
    }else {
        $pengambilan='Belum diambil';
    }

    $pdf->Cell(50,6,'Pengambilan',0,0);
    $pdf->Cell(20,6,': '.$pengambilan,0,1);

    $pdf->Cell(10,7,'',0,1);

    $pdf->SetFont('Arial','B',10);

    $pdf->Cell(8,6,'No',1,0,'C');
    $pdf->Cell(64,6,'layanan',1,0,'C');
    $pdf->Cell(22,6,'Tarif',1,0,'C');
    $pdf->Cell(20,6,'Berat',1,0,'C');
    $pdf->Cell(30,6,'Masuk',1,0,'C');
    $pdf->Cell(30,6,'Selesai',1,0,'C');
    $pdf->Cell(22,6,'Jam',1,1,'C');

    $pdf->SetFont('Arial','',10);

    $no=1;
    $total=0;

    $pdf->Cell(8,9,$no,1,0,'C');
    $pdf->Cell(64,9,$data['nama_layanan']." (".$data['nama_jenis_layanan'].")",1,0,'C');
    $pdf->Cell(22,9,'Rp. '.number_format($data['tarif'],0,',','.'),1,0,'C');
    $pdf->Cell(20,9,$data['berat'].' KG',1,0,'C');
    $pdf->Cell(30,9, date("d/m/Y",strtotime($data['tanggal_masuk'])),1,0,'C');
    $pdf->Cell(30,9, date("d/m/Y",strtotime($data['tanggal_selesai'])),1,0,'C');
    $pdf->Cell(22,9, date("H:i",strtotime($data['tanggal_transaksi'])).' WIB',1,1,'C');

    $pdf->Cell(10,7,'',0,1);
    $pdf->SetFont('Arial','B',15);
    $pdf->Cell(0,7,'Total Bayar : '.'Rp. '.number_format($data['total_biaya'],0,',','.'),0,1,'R');
     
    $pdf->Output();
?>