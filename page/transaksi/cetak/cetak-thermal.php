<?php
    include '../../../config/database.php';
    $query = mysqli_query($kon, "select * from profil_aplikasi order by nama_aplikasi desc limit 1");    
    $row = mysqli_fetch_array($query);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="style.css">
        <title>Print Receipt</title>
        <style>
            * {
                font-size: 12px;
                font-family: 'Times New Roman';
            }

            td,
            th,
            tr,
            table {
                border-top: 1px solid black;
                border-collapse: collapse;
            }

            td.description,
            th.description {
                width: 70px;
                max-width: 70px;
            }

            td.quantity,
            th.quantity {
                width: 20px;
                max-width: 20px;
                word-break: break-all;
            }

            td.price,
            th.price {
                width: 65px;
                max-width: 65px;
                word-break: break-all;
            }

            .centered {
                text-align: center;
                align-content: center;
            }

            .ticket {
                width: 155px;
                max-width: 155px;
            }

            img {
                max-width: inherit;
                width: inherit;
            }

            @media print {
                .hidden-print,
                .hidden-print * {
                    display: none !important;
                }
            }
        </style>
    </head>
    <body onload="window.print();">
        <div class="ticket">
            <p class="centered"><b><?php echo strtoupper($row['nama_aplikasi']);?></b>
                <br><?php echo $row['alamat'].', Telp '.$row['no_telp'];?>
            </p>
            <?php
                $no_invoice=$_GET['no_invoice'];
                $query = mysqli_query($kon, "SELECT * from transaksi t inner join layanan p on p.id_layanan=t.id_layanan inner join jenis_layanan j on j.id_jenis_layanan=t.id_jenis_layanan where t.no_invoice='$no_invoice'");    
                $data = mysqli_fetch_array($query);
                $no_invoice=$data['no_invoice'];
            ?>
            <p>Invoice # <?php echo  $no_invoice; ?><br>
                Tgl <?php echo date('d/m/Y', strtotime($data["tanggal_transaksi"]));?>, <?php echo date('H:i', strtotime($data["tanggal_transaksi"]));?> WIB
            </p>
            <P>
                Layanan <?php echo $data['nama_layanan']; ?> - <?php echo $data['nama_jenis_layanan']; ?>
            </P>

            <table>
                <thead>
                    <tr>
                        <th class="quantity">B</th>
                        <th class="description">Biaya</th>
                        <th class="price">Harga</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td><?php echo $data['berat']; ?></td>
                        <td>Rp. <?php echo number_format($data['tarif'],0,',','.'); ?>/KG</td>
                        <td class="price">Rp. <?php echo number_format($data['tarif']*$data['berat'],0,',','.'); ?> </td>
                    </tr>

                </tbody>
            </table>
            <p class="centered">Terima kasih atas kepercayaan anda menggunakan jasa kami</p>
        </div>
    </body>
</html>