<!DOCTYPE html>
<html>
<head>
  <!-- Custom styles for this template -->
  <link href="../../../assets/css/sb-admin-2.min.css" rel="stylesheet">
</head>
    <body onload="window.print();">
        <?php
        include '../../../config/database.php';
   
        $query = mysqli_query($kon, "select * from profil_aplikasi order by nama_aplikasi desc limit 1");    
        $row = mysqli_fetch_array($query);
        ?>
        <div class="container-fluid">
            <div class="card">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-sm-1 text-left">
                    <img src="../../../page/aplikasi/logo/<?php echo $row['logo']; ?>" width="95px" alt="brand"/>
                    </div>
                    <div class="col-sm-11 text-left">
                        <h3><?php echo strtoupper($row['nama_aplikasi']);?></h3>
                        <h6><?php echo $row['alamat'].', Telp '.$row['no_telp'];?></h6>
                        <h6><?php echo $row['website'];?></h6>
                    </div>
                </div>
            </div>
            <?php
                $no_invoice=$_GET['no_invoice'];
                $query = mysqli_query($kon, "SELECT * from transaksi t inner join layanan p on p.id_layanan=t.id_layanan inner join jenis_layanan j on j.id_jenis_layanan=t.id_jenis_layanan where t.no_invoice='$no_invoice'");    

                $data = mysqli_fetch_array($query);
                $no_invoice=$data['no_invoice'];
            ?>
            <div class="card-body">
                <!--rows -->   
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td>No Invoice</td>
                                        <td>: <?php echo $data['no_invoice'];?></td>
                                        <input type="hidden" name="no_invoice" id="no_invoice" value="<?php echo $data['no_invoice'];; ?>" >
                                    </tr>
                                    <tr>
                                        <td>Tanggal Transaksi</td>
                                        <td>: <?php echo date('d/m/Y', strtotime($data["tanggal_transaksi"]));?></td>
                                    </tr>
                                    <tr>
                                        <td>Pelanggan</td>
                                        <td>: <?php echo $data['nama_pelanggan'];?></td>
                                    </tr>
                                    <tr>
                                        <td>No Telp</td>
                                        <td>: <?php echo $data['no_telp'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>: <?php echo $data['alamat'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Pembayaran</td>
                                        <td>: <?php echo $data['status_bayar'] == 1 ? "Telah dibayar" : "Belum bayar"; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Pengambilan</td>
                                        <td>: <?php echo $data['status_pengambilan'] == 1 ? "Telah diambil" : "Belum diambil"; ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-8">
                        
                        </div>
                    </div>
                    <!--rows -->
                    <div>
                        <div class="col-sm-12">
                            <table class="table table-bordered">
                                <thead class="text-center">
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">layanan</th>
                                    <th rowspan="2">Jenis</th>
                                    <th rowspan="2">Tarif</th>
                                    <th rowspan="2">Berat</th>
                                    <th colspan="3" >Waktu</th>
                                </tr>
                                <tr>
                                    <th>Mulai</th>
                                    <th>Selesai</th>
                                    <th>Jam</th>
                                </tr>
                                </thead>
                                <tbody class="text-center">
                                <tr>
                                    <td><?php echo 1; ?></td>
                                    <td><?php echo $data['nama_layanan']; ?></td>
                                    <td><?php echo $data['nama_jenis_layanan']; ?></td>
                                    <td>Rp. <?php echo number_format($data['tarif'],0,',','.'); ?></td>
                                    <td><?php echo $data['berat']; ?> KG</td>
                                    <td><?php echo date("d/m/Y",strtotime($data['tanggal_masuk'])); ?></td>
                                    <td><?php echo date("d/m/Y",strtotime($data['tanggal_selesai'])); ?></td>
                                    <td><?php echo date("H:i",strtotime($data['tanggal_transaksi'])); ?> WIB</td>
                                </tr>
                                </tbody>
                            </table><br>
                            <span class="text-right"><h3>Total Bayar : Rp. <?php echo number_format($data['total_biaya'],0,',','.'); ?></h3></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>