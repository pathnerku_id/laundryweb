<script>
$('title').text('Daftar Transaksi Laundry');
</script>

<div class="container-fluid">
    <?php
  
    if (isset($_GET['hapus'])) {
        if ($_GET['hapus']=='berhasil'){
            echo"<div class='alert alert-success'>Transaksi telah di hapus</div>";
        }else if ($_GET['hapus']=='gagal'){
            echo"<div class='alert alert-danger'><strong>Gagal!</strong> transaksi gagal di hapus</div>";
        }    
    }

?>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h4>Daftar Transaksi</h4>
        </div>
        <div class="card-body">
            <!-- Tabel daftar transaksi -->
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>No Invoice</th>
                            <th>Pelanggan</th>
                            <th>Layanan</th>
                            <th>Total Biaya</th>
                            <th>Pembayaran</th>
                            <th>Metode Pemesanan</th>
                            <th>Pengambilan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                // include database
                include 'config/database.php';
                if ($_SESSION["level"]=="Admin"){
                    $id_pengguna=$_SESSION["id_pengguna"];
                    $sql="SELECT * from transaksi t inner join layanan l on l.id_layanan=t.id_layanan inner join jenis_layanan j on j.id_jenis_layanan=t.id_jenis_layanan  where t.id_pengguna=$id_pengguna order by t.id_transaksi desc";
                }else {
                    $sql="SELECT * from transaksi t inner join layanan l on l.id_layanan=t.id_layanan inner join jenis_layanan j on j.id_jenis_layanan=t.id_jenis_layanan  order by t.id_transaksi desc";
                }

          
                $hasil=mysqli_query($kon,$sql);
                $no=0;
                //Menampilkan data dengan perulangan while
                while ($data = mysqli_fetch_array($hasil)):
                $no++;
            ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo date('d/m/Y', strtotime($data["tanggal_transaksi"])); ?></td>
                            <td><?php echo $data['no_invoice']; ?></td>
                            <td><?php echo $data['nama_pelanggan']; ?></td>
                            <td><?php echo $data['nama_layanan']; ?> (<?php echo $data['nama_jenis_layanan']; ?>)</td>
                            <td>Rl. <?php echo number_format($data['total_biaya'],0,',','.'); ?></td>
                            <td><?php echo $data['status_bayar'] == 1 ? "<span class='badge badge-success'>Telah dibayar</span>" : "<span class='badge badge-warning'>Belum bayar</span>"; ?>
                            </td>
                            <td><?php echo $data['metode_pengiriman'] == 1 ? "<span class='badge badge-success'>Kirim ke rumah</span>" : "<span class='badge badge-warning'>Ambil di toko</span>"; ?>
                            </td>
                            <td><?php echo $data['status_pengambilan'] == 1 ? "<span class='badge badge-success'>Telah diambil</span>" : "<span class='badge badge-warning'>Belum diambil</span>"; ?>
                            </td>
                            <td>
                                <a href="index.php?page=detail-transaksi&no_invoice=<?php echo $data['no_invoice']; ?>"
                                    class="btn btn-success btn-circle"
                                    id_transaksi="<?php echo $data['id_transaksi']; ?>"><i
                                        class="fas fa-mouse-pointer"></i></a>
                                <?php if ($_SESSION["level"]=="Super Admin"): ?>
                                <button class="btn-hapus-transaksi btn btn-danger btn-circle"
                                    id_transaksi="<?php echo $data['id_transaksi']; ?>"
                                    no_invoice="<?php echo $data['no_invoice']; ?>"><i
                                        class="fas fa-trash"></i></button>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <!-- bagian akhir (penutup) while -->
                        <?php endwhile; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>





<script>
// Hapus transaksi
$('.btn-hapus-transaksi').on('click', function() {

    var id_transaksi = $(this).attr("id_transaksi");

    konfirmasi = confirm("Yakin ingin menghapus?")

    if (konfirmasi) {
        $.ajax({
            url: "page/transaksi/hapus-transaksi.php",
            method: 'post',
            data: {
                id_transaksi: id_transaksi
            },
            success: function(data) {
                window.location.href = 'index.php?page=transaksi&hapus=berhasil';
            }
        });
    }
});
</script>