<?php
    session_start();
    include '../../config/database.php';

    //Memulai transaksi
    mysqli_query($kon,"START TRANSACTION");

    $id_transaksi=$_POST["id_transaksi"];
    $hapus_transaksi=mysqli_query($kon,"delete from transaksi where id_transaksi=$id_transaksi");

    //Menyimpan aktivitas
    $id_pengguna=$_SESSION['id_pengguna'];
    $waktu=date("Y-m-d H:i:s");
    $log_aktivitas="Hapus Transaksi ID #$id_transaksi";
    $simpan_aktivitas=mysqli_query($kon,"insert into log_aktivitas (waktu,aktivitas,id_pengguna) values ('$waktu','$log_aktivitas','$id_pengguna')");

    //Kondisi apakah berhasil atau tidak dalam mengeksekusi query diatas
    if ($hapus_transaksi and $simpan_aktivitas) {
        mysqli_query($kon,"COMMIT");
    }
    else {
        mysqli_query($kon,"ROLLBACK");
    }

?>