<script>
$('title').text('Buat Transaksi Baru');
</script>
<div class="container-fluid">
    <form method="post" id="form_transaksi_baru">
        <?php
      //Validasi untuk menampilkan pesan pemberitahuan saat user menambah layanan
      if (isset($_GET['tambah'])) {
          //Mengecek nilai variabel tambah
          if ($_GET['tambah']=='berhasil'){
              echo"<div class='alert alert-success'><strong>Berhasil!</strong> layanan telah ditambah!</div>";
          }else if ($_GET['tambah']=='gagal'){
              echo"<div class='alert alert-danger'><strong>Gagal!</strong> layanan gagal ditambahkan!</div>";
          }    
      }

      if (isset($_GET['edit'])) {
        //Mengecek nilai variabel edit yang telah di enskripsi dengan method md5()
        if ($_GET['edit']=='eafd30500d78beb7a754323b4022b763'){
            echo"<div class='alert alert-success'><strong>Berhasil!</strong> layanan telah diupdate!</div>";
        }else if ($_GET['edit']=='35480799dafaadb408ef10e6396cefd7'){
            echo"<div class='alert alert-danger'><strong>Gagal!</strong> layanan gagal diupdate!</div>";
        }    
      }
      if (isset($_GET['hapus'])) {
        //Mengecek nilai variabel edit yang telah di enskripsi dengan method md5()
        if ($_GET['hapus']=='eafd30500d78beb7a754323b4022b763'){
            echo"<div class='alert alert-success'><strong>Berhasil!</strong> layanan telah dihapus!</div>";
        }else if ($_GET['hapus']=='35480799dafaadb408ef10e6396cefd7'){
            echo"<div class='alert alert-danger'><strong>Gagal!</strong> layanan gagal dihapus!</div>";
        }    
      }
  ?>

        <?php
      // mengambil data barang dengan kode paling besar
      include 'config/database.php';
      $query = mysqli_query($kon, "SELECT max(id_transaksi) as kodeTerbesar FROM transaksi");
      $data = mysqli_fetch_array($query);
      $id_transaksi = $data['kodeTerbesar'];
      $id_transaksi++;
      $huruf = "T";
      $kodetransaksi = $huruf . sprintf("%04s", $id_transaksi);
  ?>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-sm-6">
                        <h4>Transaksi Baru</h4>
                    </div>
                    <div class="col-sm-6">
                        <h4 class="text-right">No Invoice <?php echo $kodetransaksi; ?></h4>
                        <input type="hidden" name="no_invoice" id="no_invoice" value="<?php echo $kodetransaksi; ?>">
                    </div>
                </div>

            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="card mb-4">
                            <div class="card-header" id="judul_2">Data Pelanggan</div>
                            <div class="card-body" id="tampil_jenis_layanan">
                                <div class="form-group">
                                    <label for="sel1">Pelanggan:</label>
                                    <select class="form-control" id="pilih_pelanggan" name="id_pelanggan">
                                        <option value="">Pilih</option>
                                        <?php
                      include 'config/database.php';
                      $sql="select * from pelanggan order by id_pelanggan asc";
                      $hasil=mysqli_query($kon,$sql);
                      while ($data = mysqli_fetch_array($hasil)):
                  ?>
                                        <option value="<?php echo $data['id_pelanggan']; ?>">
                                            <?php echo $data['nama_pelanggan']; ?></option>
                                        <?php endwhile; ?>
                                    </select>
                                </div>
                                <div id="tampil_data_pelanggan">
                                    <!--Data pelanggan akan diload disini dengan AJAX (tidak terlihat karena menggunakan type hidden) -->
                                </div>
                                <div class="form-group">
                                    <label>Nama:</label>
                                    <input type="text" class="form-control" id="nama_pelanggan" name="nama_pelanggan">

                                </div>
                                <div class="form-group">
                                    <label>No Telp:</label>
                                    <input type="number" class="form-control" id="no_telp" name="no_telp">
                                </div>
                                <div class="form-group">
                                    <label>Alamat:</label>
                                    <textarea class="form-control" rows="3" id="alamat" name="alamat"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="card mb-4">
                            <div class="card-header">Pilih Layanan</div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <select class="form-control" id="pilih_layanan" name="id_layanan">
                                                <?php
                          include 'config/database.php';
                          $sql="select * from layanan order by id_layanan asc";
                          $hasil=mysqli_query($kon,$sql);
                          while ($data = mysqli_fetch_array($hasil)):
                        ?>
                                                <option value="<?php echo $data['id_layanan']; ?>">
                                                    <?php echo $data['nama_layanan']; ?></option>
                                                <?php endwhile; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <select class="form-control" id="jenis_layanan" name="id_jenis_layanan">

                                                <!--Item jenis layanan akan tampil berdasarkan layanan yang dipilih -->
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group" id="detail_jenis_layanan">
                                    <!--Detail jenis layanan akan ditampilkan disini menggunakan AJAX-->

                                </div>
                                <div class="form-group">
                                    <label>Berat (KG):</label>
                                    <input type="number" class="form-control" id="berat" name="berat">
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Masuk:</label>
                                    <input type="date" class="form-control" id="tanggal_masuk" name="tanggal_masuk">
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Selesai:</label>
                                    <input type="date" class="tanggal_selesai form-control" disabled>
                                    <input type="hidden" class="tanggal_selesai form-control" name="tanggal_selesai">
                                </div>
                                <div class="form-group">
                                    <label>Keterangan:</label>
                                    <textarea class="form-control" rows="3" name="keterangan"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Status Pembayaran:</label>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="status_bayar"
                                            name="status_bayar" value="0">
                                        <label class="custom-control-label" for="status_bayar">Belum Bayar</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="status_bayar2"
                                            name="status_bayar" value="1">
                                        <label class="custom-control-label" for="status_bayar2">Telah Bayar</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h3>Rp.<span id="total_biaya"></span></h3>
                                    <input type="hidden" class="form-control" id="tot_biaya" name="tot_biaya">
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-success" id="buat_transaksi"
                                        name="buat_transaksi" disabled>Buat Transaksi</button>
                                    <br><span><small>Masukan berat untuk mengaktifkan tombol</small></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
$(document).ready(function() {
    jenis_layanan();
    tabel_layanan();

    $('#pilih_pelanggan').bind('change', function() {
        var pelanggan = $("#pilih_pelanggan").val();

        if ($('#pilih_pelanggan').val() == '') {
            $('#nama_pelanggan').val('');
            $('#no_telp').val('');
            $('#alamat').val('');
        } else {
            $.ajax({
                method: 'POST',
                url: 'page/transaksi/ambil-data-pelanggan.php',
                data: {
                    pelanggan: pelanggan
                },
                success: function(data) {
                    $('#tampil_data_pelanggan').html(data);
                    var sesi_nama_pelanggan = $("#sesi_nama_pelanggan").val();
                    var sesi_no_telp_pelanggan = $("#sesi_no_telp_pelanggan").val();
                    var sesi_alamat_pelanggan = $("#sesi_alamat_pelanggan").val();
                    $('#nama_pelanggan').val(sesi_nama_pelanggan);
                    $('#no_telp').val(sesi_no_telp_pelanggan);
                    $('#alamat').val(sesi_alamat_pelanggan);
                }
            });
        }
    });


    $('#pilih_layanan').bind('change', function() {
        jenis_layanan();
        tabel_layanan();

    });


    $('#jenis_layanan').bind('change', function() {
        tabel_layanan();
    });


    $('#tanggal_masuk').bind('change', function() {
        tanggal_akhir();
    });

    $("#berat").bind('keyup', function() {
        hitung_total_biaya();
    });

    $("#berat").bind('change', function() {
        hitung_total_biaya();
    });

    $("#tarif").bind('change', function() {
        hitung_total_biaya();
    });


    function jenis_layanan() {
        var layanan = $("#pilih_layanan").val();
        $.ajax({
            url: 'page/transaksi/ambil-data-jenis-layanan.php',
            method: "POST",
            data: {
                layanan: layanan
            },
            async: false,
            dataType: 'json',
            success: function(data) {
                var html = '';
                var i;
                for (i = 0; i < data.length; i++) {
                    html += '<option value=' + data[i].id_jenis_layanan + '>' + data[i]
                        .nama_jenis_layanan + '</option>';
                }
                $('#jenis_layanan').html(html);

            }
        })
    }

    function tabel_layanan() {
        var layanan = $("#pilih_layanan").val();
        var jenis_layanan = $("#jenis_layanan").val();

        $.ajax({
            method: 'POST',
            dataType: 'html',
            url: 'page/transaksi/detail-jenis-layanan.php',
            data: {
                jenis_layanan: jenis_layanan,
                layanan: layanan
            },
            success: function(data) {
                $('#detail_jenis_layanan').html(data);
                hitung_total_biaya();
            }
        });
    }

    function hitung_total_biaya() {
        var tarif = $('#tarif').val();
        var berat = $('#berat').val();
        var total_biaya = tarif * berat;

        var reverse = total_biaya.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join('.').split('').reverse().join('');

        $('#tot_biaya').val(total_biaya);
        $('#total_biaya').text(ribuan);

        if (total_biaya >= 1) {
            document.getElementById("buat_transaksi").disabled = false;
        } else {
            document.getElementById("buat_transaksi").disabled = true;
        }
    }


    function tanggal_akhir() {
        var tanggal_masuk = $("#tanggal_masuk").val();
        var estimasi_waktu = $("#estimasi_waktu").text();
        $.ajax({
            method: 'POST',
            url: 'page/transaksi/tanggal-selesai.php',
            data: {
                tanggal_masuk: tanggal_masuk,
                estimasi_waktu: estimasi_waktu
            },
            success: function(data) {
                $('.tanggal_selesai').val(data);
            }
        });
    }

    $('#buat_transaksi').click(function() {
        if ($('#tanggal_masuk').val() == '') {
            alert('Tanggal masuk belum dimasukan');
            exit;
        }
        if (($('#status_bayar').prop("checked") == false && $('#status_bayar2').prop("checked") ==
                false)) {
            alert('Status pembayaran harus dipilih');
            exit;
        }

        var data = $('#form_transaksi_baru').serialize();
        console.log(data);
        var no_invoice = $("#no_invoice").val();
        $.ajax({
            type: 'POST',
            url: "page/transaksi/simpan-transaksi.php",
            data: data,
            cache: false,
            success: function(data) {
                window.location.href = 'index.php?page=detail-transaksi&no_invoice=' +
                    no_invoice;
            }
        });
    });


});
</script>

<div id='ajax-wait'>
    <div class="spinner-border text-info"></div>
</div>

<style>
#ajax-wait {
    display: none;
    position: fixed;
    z-index: 1999
}
</style>
<script>
$(document).ready(function() {
    loading();
});

//Fungsi untuk efek loading
function loading() {
    $(document).ajaxStart(function() {
            $("#ajax-wait").css({
                left: ($(window).width() - 32) / 2 + "px", // 32 = lebar gambar
                top: ($(window).height() - 32) / 2 + "px", // 32 = tinggi gambar
                display: "block"
            })
        })
        .ajaxComplete(function() {
            $("#ajax-wait").fadeOut();
        });
}
</script>