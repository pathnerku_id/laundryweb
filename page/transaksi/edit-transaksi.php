<?php
    session_start();
    include '../../config/database.php';
    //Memulai transaksi
    mysqli_query($kon,"START TRANSACTION");

    //Fungsi untuk mencegah inputan karakter yang tidak sesuai
    function input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    $no_invoice=input($_POST['no_invoice']);
    $id_pelanggan=input($_POST['id_pelanggan']);
    $nama_pelanggan=input($_POST['nama_pelanggan']);
    $no_telp=input($_POST['no_telp']);
    $alamat=input($_POST['alamat']);
    
    $id_layanan=input($_POST['id_layanan']);
    $id_jenis_layanan=input($_POST['id_jenis_layanan']);
    $berat=input($_POST['berat']);
    $tanggal_masuk=$_POST['tanggal_masuk'];
    $jam_sekarang=date("H:i");
    $tanggal_transaksi=date("Y-m-d H:i",strtotime($tanggal_masuk.$jam_sekarang));
    $tanggal_selesai=$_POST['tanggal_selesai'];
    $keterangan=input($_POST['keterangan']);
    $tot_biaya=input($_POST['tot_biaya']);
    $status_bayar=input($_POST['status_bayar']);
    $status_pengambilan=input($_POST['status_pengambilan']);
    
    
    //Query input menginput data kedalam tabel anggota
    $sql="update transaksi set
    id_pelanggan='$id_pelanggan',
    nama_pelanggan='$nama_pelanggan',
    no_telp='$no_telp',
    alamat='$alamat',
    id_layanan='$id_layanan',
    id_jenis_layanan='$id_jenis_layanan',
    berat='$berat',
    tanggal_transaksi='$tanggal_transaksi',
    tanggal_masuk='$tanggal_masuk',
    tanggal_selesai='$tanggal_selesai',
    keterangan='$keterangan',
    total_biaya='$tot_biaya',
    status_bayar='$status_bayar',
    status_pengambilan='$status_pengambilan'
    where no_invoice='$no_invoice'";

    //Mengeksekusi/menjalankan query 
    $update_transaksi=mysqli_query($kon,$sql);

    //Menyimpan aktivitas
    $id_pengguna=$_SESSION['id_pengguna'];
    $waktu=date("Y-m-d H:i:s");
    $log_aktivitas="Update transaksi no invoice #$no_invoice";
    $simpan_aktivitas=mysqli_query($kon,"insert into log_aktivitas (waktu,aktivitas,id_pengguna) values ('$waktu','$log_aktivitas','$id_pengguna')");
    
    //Kondisi apakah berhasil atau tidak dalam mengeksekusi query diatas
    if ($update_transaksi and $simpan_aktivitas) {
        mysqli_query($kon,"COMMIT");
    }
    else {
        mysqli_query($kon,"ROLLBACK");
    }

?>