<script>
    $('title').text('Detail Transaksi Laundry');
</script>
<?php
    include 'config/database.php';
    $no_invoice=$_GET['no_invoice'];
    $query = mysqli_query($kon, "SELECT * from transaksi t inner join layanan p on p.id_layanan=t.id_layanan inner join jenis_layanan j on j.id_jenis_layanan=t.id_jenis_layanan where t.no_invoice='$no_invoice'");    
    $data = mysqli_fetch_array($query);
    $no_invoice=$data['no_invoice'];
?>
<form method="post" id="form_transaksi_edit">
  <div class="container-fluid">
  <?php
      //Validasi untuk menampilkan pesan pemberitahuan saat user menambah transaksi
      if (isset($_GET['edit'])) {
          //Mengecek nilai variabel add yang telah di enskripsi dengan method md5()
          if ($_GET['edit']=='berhasil'){
              echo"<div class='alert alert-success'>Edit data transaksi berhasil</div>";
          }   
      }
  ?>
    <div class="card shadow mb-4">
      <div class="card-header py-3">
      <div class="row">
          <div class="col-sm-6">
              <h4>Detail Transaksi</h4>
          </div>
          <div class="col-sm-6 text-right">
              <h6><?php echo $data['status_bayar'] == 1 ? "<span class='badge-pill badge-success'>Telah dibayar</span>" : "<span class='badge-pill badge-warning'>Belum bayar</span>"; ?>
                <?php 
                  echo $data['status_pengambilan'] == 1 ? "<span class='badge-pill badge-success'>Telah diambil</span>" : "<span class='badge-pill badge-warning'>Belum diambil</span>";
                ?>
             </h6>
          </div>
        </div>
      </div>
      <div class="card-body">
        <!--rows -->   
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <table class="table">
                            <tbody>
                            <tr>
                                <td>No Invoice</td>
                                <td>: <?php echo $data['no_invoice'];?></td>
                                <input type="hidden" name="no_invoice" id="no_invoice" value="<?php echo $data['no_invoice'];; ?>" >
                            </tr>
                            <tr>
                                <td>Tanggal Transaksi</td>
                                <td>: <?php echo date('d/m/Y', strtotime($data["tanggal_transaksi"]));?></td>
                            </tr>
                            <tr>
                                <td>Pelanggan</td>
                                <td>: <?php echo $data['nama_pelanggan'];?></td>
                            </tr>
                            <tr>
                                <td>No Telp</td>
                                <td>: <?php echo $data['no_telp'];?></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>: <?php echo $data['alamat'];?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-sm-8">
                
                </div>
            </div>
            <!--rows -->
            <div>
                <div class="col-sm-12">
                    <table class="table table-bordered">
                        <thead class="text-center">
                        <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2">Layanan</th>
                            <th rowspan="2">Jenis</th>
                            <th rowspan="2">Tarif</th>
                            <th rowspan="2">Berat</th>
                            <th rowspan="2">Total Biaya</th>
                            <th colspan="3" >Waktu</th>
                        </tr>
                        <tr>
                            <th>Mulai</th>
                            <th>Selesai</th>
                            <th>Jam</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        <tr>
                            <td><?php echo 1; ?></td>
                            <td><?php echo $data['nama_layanan']; ?></td>
                            <td><?php echo $data['nama_jenis_layanan']; ?></td>
                            <td>Rp. <?php echo number_format($data['tarif'],0,',','.'); ?>/KG</td>
                            <td><?php echo $data['berat']; ?> KG</td>
                            <td>Rp. <?php echo number_format($data['total_biaya'],0,',','.'); ?></td>
                            <td><?php echo date("d/m/Y",strtotime($data['tanggal_masuk'])); ?></td>
                            <td><?php echo date("d/m/Y",strtotime($data['tanggal_selesai'])); ?></td>
                            <td><?php echo date("H:i",strtotime($data['tanggal_transaksi'])); ?> WIB</td>
                        </tr>
                  
                        </tbody>
                    </table>
                </div>
                <?php if ($_SESSION["level"]=="Super Admin" || $_SESSION["level"]=="Admin"): ?>
                <button type="button" data-toggle="modal" data-target="#editData" class="btn btn-warning"><i class="fa fa-address-book"></i> Edit Data</button>
                <?php endif; ?>
                <!-- Tombol cetak invoice -->
                <a href="page/transaksi/cetak/cetak-transaksi.php?no_invoice=<?php echo $data['no_invoice'];?>" target='blank' class="btn btn-primary btn-icon-split"><span class="text"><i class="fas fa-print"></i> Cetak Invoice</span></a>
                <a href="page/transaksi/cetak/cetak-thermal.php?no_invoice=<?php echo $data['no_invoice'];?>" target='blank' class="btn btn-success btn-icon-split"><span class="text"><i class="fas fa-print"></i> Printer Thermal</span></a>
                <a href="page/transaksi/cetak/pdf/detail-transaksi-pdf.php?no_invoice=<?php echo $data['no_invoice'];?>" target='blank' class="btn btn-danger btn-icon-pdf"><span class="text"><i class="fas fa-file-pdf"></i> Export PDF</span></a>
            </div>
        <!--rows -->
        </div>
    </div>
  </div>

  <!-- The Modal -->
  <div class="modal fade" id="editData">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit Transaksi</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
        <div class="row">
          <div class="col-sm-5">
            <div class="card mb-4">
              <div class="card-header" id="judul_2">Data Pelanggan</div>
                <div class="card-body" id="tampil_jenis_layanan">
                  <div class="form-group">
                    <label for="sel1">Pelanggan:</label>
                    <select class="form-control" id="pilih_pelanggan" name="id_pelanggan">
                    <option value="">Pilih</option>
                    <?php
                        include 'config/database.php';
                        $sql="select * from pelanggan order by id_pelanggan asc";
                        $hasil=mysqli_query($kon,$sql);
                        while ($ambil = mysqli_fetch_array($hasil)):
                    ?>
                        <option value="<?php echo $ambil['id_pelanggan']; ?>"><?php echo $ambil['nama_pelanggan']; ?></option>
                    <?php endwhile; ?>
                    </select>
                  </div>
                  <div id="tampil_data_pelanggan"> 
                  <!--Data pelanggan akan diload disini dengan AJAX (tidak terlihat karena menggunakan type hidden) -->
                  </div>
                  <div class="form-group">
                    <label>Nama:</label>
                    <input type="text" class="form-control" id="nama_pelanggan" value="<?php echo $data['nama_pelanggan'];?>" name="nama_pelanggan">
                          
                  </div>
                  <div class="form-group">
                    <label>No Telp:</label>
                    <input type="number" class="form-control" id="no_telp" value="<?php echo $data['no_telp'];?>" name="no_telp">
                  </div>
                  <div class="form-group">
                    <label>Alamat:</label>
                    <textarea class="form-control" rows="3" id="alamat" name="alamat"><?php echo $data['alamat'];?></textarea>
                  </div>
                </div>
            </div>
            </div>
            <div class="col-sm-7">
              <div class="card mb-4">
                <div class="card-header">Pilih Layanan</div>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <select class="form-control" id="pilih_layanan" name="id_layanan">
                          <?php
                            include 'config/database.php';
                            $sql="select * from layanan order by id_layanan asc";
                            $hasil=mysqli_query($kon,$sql);
                            while ($ambil = mysqli_fetch_array($hasil)):
                          ?>
                            <option <?php if ($data['id_layanan']==$ambil['id_layanan']) echo "selected"; ?> value="<?php echo $ambil['id_layanan']; ?>"><?php echo $ambil['nama_layanan']; ?></option>
                          <?php endwhile; ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <select class="form-control" id="jenis_layanan" name="id_jenis_layanan">
                            <!--Item jenis layanan akan tampil berdasarkan layanan yang dipilih -->
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group" id="detail_jenis_layanan">
                        <!--Detail jenis layanan akan ditampilkan disini -->
                    </div>
                    <div class="form-group">
                      <label>Berat (KG):</label>
                      <input type="number" class="form-control" id="berat" value="<?php echo $data['berat'];?>" name="berat">
                    </div>
                    <div class="form-group">
                      <label>Tanggal Masuk:</label>
                      <input type="date" class="form-control" id="tanggal_masuk" value="<?php echo $data['tanggal_masuk'];?>" name="tanggal_masuk">
                    </div>
                    <div class="form-group">
                      <label>Tanggal Selesai:</label>
                      <input type="date" class="tanggal_selesai form-control" value="<?php echo $data['tanggal_selesai'];?>" disabled>
                      <input type="hidden" class="tanggal_selesai form-control" value="<?php echo $data['tanggal_selesai'];?>" name="tanggal_selesai" >
                    </div>

                    <div class="form-group">
                      <label>Keterangan:</label>
                      <textarea class="form-control" rows="3" name="keterangan"><?php echo $data['keterangan'];?></textarea>
                    </div>
                    <div class="form-group">
                    <label>Status Pembayaran:</label>
                      <div class="form-check-inline">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input"  <?php if (isset($data['status_bayar']) && $data['status_bayar']==0) echo "checked"; ?> name="status_bayar" value="0">Belum Bayar
                        </label>
                      </div>
                      <div class="form-check-inline">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" <?php if (isset($data['status_bayar']) && $data['status_bayar']==1) echo "checked"; ?> name="status_bayar" value="1">Telah Bayar
                        </label>
                      </div>
                    </div>

                    <div class="form-group">
                    <label>Status Pengambilan:</label>
                      <div class="form-check-inline">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input"  <?php if (isset($data['status_pengambilan']) && $data['status_pengambilan']==0) echo "checked"; ?> name="status_pengambilan" value="0">Belum Diambil
                        </label>
                      </div>
                      <div class="form-check-inline">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" <?php if (isset($data['status_pengambilan']) && $data['status_pengambilan']==1) echo "checked"; ?> name="status_pengambilan" value="1">Telah Diambil
                        </label>
                      </div>
                    </div>

                    <div class="form-group">
                          <h3>Rp.<span id="total_biaya"><?php echo number_format($data['total_biaya'],0,',','.'); ?></span></h3>
                          <input type="hidden" class="form-control"  id="tot_biaya" name="tot_biaya">
                    </div>
                    <div class="form-group">
                    <button type="button" class="btn btn-warning" id="edit_transaksi" name="edit_transaksi" disabled>Edit Transaksi</button>
                    </div>
                  </div>
              </div>
            </div>
        </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
  </form>


<script>
  $(document).ready(function(){
    jenis_layanan();
    tabel_layanan();

    $('#pilih_pelanggan').bind('change', function () {
      var pelanggan=$("#pilih_pelanggan").val();

      if( $('#pilih_pelanggan').val()=='') {
            $('#nama_pelanggan').val('');
            $('#no_telp').val('');
            $('#alamat').val('');
      }else {
          $.ajax({
          method: 'POST',
          url: 'page/transaksi/ambil-data-pelanggan.php',
          data: {pelanggan:pelanggan},
          success	: function(data){
            $('#tampil_data_pelanggan').html(data);
            var sesi_nama_pelanggan=$("#sesi_nama_pelanggan").val();
            var sesi_no_telp_pelanggan=$("#sesi_no_telp_pelanggan").val();
            var sesi_alamat_pelanggan=$("#sesi_alamat_pelanggan").val();
            $('#nama_pelanggan').val(sesi_nama_pelanggan);
            $('#no_telp').val(sesi_no_telp_pelanggan);
            $('#alamat').val(sesi_alamat_pelanggan);
          }
          });
      }
    });

    $('#pilih_layanan').bind('change', function () {
      jenis_layanan();
      tabel_layanan();
    
    });

    $('#jenis_layanan').bind('change', function () {
      tabel_layanan();
    });

    function jenis_layanan(){
      var layanan=$("#pilih_layanan").val();
      $.ajax({
            url: 'page/transaksi/ambil-data-jenis-layanan.php',
            method : "POST",
            data : {layanan:layanan},
            async : false,
            dataType : 'json',
            success: function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<option value='+data[i].id_jenis_layanan+'>'+data[i].nama_jenis_layanan+'</option>';
                }
                $('#jenis_layanan').html(html);

            }
        })
    }

    function tabel_layanan() {
        var layanan=$("#pilih_layanan").val();
        var jenis_layanan=$("#jenis_layanan").val();
        
        $.ajax({
          method: 'POST',
          dataType: 'html',
          url: 'page/transaksi/detail-jenis-layanan.php',
          data: {jenis_layanan:jenis_layanan,layanan:layanan},
          success	: function(data){
          $('#detail_jenis_layanan').html(data);
          hitung_total_biaya();  
          }
        });
    }

    $('#tanggal_masuk').bind('change', function () {
      tanggal_akhir();
    });


    $("#berat").bind('keyup', function () {
      hitung_total_biaya();    
    });

    $("#berat").bind('change', function () {
      hitung_total_biaya();    
    });

    $("#tarif").bind('change', function () {
      hitung_total_biaya();    
    });


    function hitung_total_biaya() {
      var tarif = $('#tarif').val();
      var berat = $('#berat').val();
      var total_biaya = tarif*berat;
		
      var	reverse = total_biaya.toString().split('').reverse().join(''),
      ribuan 	= reverse.match(/\d{1,3}/g);
      ribuan	= ribuan.join('.').split('').reverse().join('');

      $('#tot_biaya').val(total_biaya);
      $('#total_biaya').text(ribuan);

      if (total_biaya>=1){
            document.getElementById("edit_transaksi").disabled = false;
        }else {
            document.getElementById("edit_transaksi").disabled = true;
        }
    }


    function tanggal_akhir(){
      var tanggal_masuk=$("#tanggal_masuk").val();
        var estimasi_waktu=$("#estimasi_waktu").text();
        $.ajax({
        method: 'POST',
        url: 'page/transaksi/tanggal-selesai.php',
        data: {tanggal_masuk:tanggal_masuk,estimasi_waktu:estimasi_waktu},
        success	: function(data){
          $('.tanggal_selesai').val(data);
        }
        });
    }

    $('#edit_transaksi').click(function(){
        if( $('#tanggal_masuk').val()=='') {
              alert('Tanggal masuk harus dipilih');
              exit;
          }
        if (($('#status_bayar').prop("checked") == false && $('#status_bayar2').prop("checked") == false)){
              alert('Status bayar harus dipilih');
              exit;
          }

        var data = $('#form_transaksi_edit').serialize();
        var no_invoice=$("#no_invoice").val();
        $.ajax({
            type	: 'POST',
            url	: "page/transaksi/edit-transaksi.php",
            data: data,
            cache	: false,
            success	: function(data){
              window.location.href = 'index.php?page=detail-transaksi&no_invoice='+no_invoice+'&edit=berhasil';
            },            
        });
    });
     
  });
</script>

<div id='ajax-wait'>
    <div class="spinner-border text-info"></div>
</div>

  <style>
      #ajax-wait {
      display: none;
      position: fixed;
      z-index: 1999
      }
  </style>
<script>

    $(document).ready( function () {
        loading();
    });

    //Fungsi untuk efek loading
    function loading(){
        $( document ).ajaxStart(function() {
        $( "#ajax-wait" ).css({
            left: ( $( window ).width() - 32 ) / 2 + "px", // 32 = lebar gambar
            top: ( $( window ).height() - 32 ) / 2 + "px", // 32 = tinggi gambar
            display: "block"
        })
        })
        .ajaxComplete( function() {
            $( "#ajax-wait" ).fadeOut();
        });
    }
</script>