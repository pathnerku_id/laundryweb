<?php
    session_start();

    //Include file koneksi, untuk koneksikan ke database
    include '../../../config/database.php';

    //Memulai transaksi
    mysqli_query($kon,"START TRANSACTION");
    
    //Fungsi untuk mencegah inputan karakter yang tidak sesuai
    function input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    $id_layanan=input($_POST["id_layanan"]);
    $kode_jenis_layanan=input($_POST["kode_jenis_layanan"]);
    $nama_jenis_layanan=input($_POST["nama_jenis_layanan"]);
    $estimasi_waktu=input($_POST["estimasi_waktu"]);
    $tarif=input($_POST["tarif"]);

    $sql="insert into jenis_layanan (kode_jenis_layanan,id_layanan,nama_jenis_layanan,estimasi_waktu,tarif) values
    ('$kode_jenis_layanan','$id_layanan','$nama_jenis_layanan','$estimasi_waktu','$tarif')";

    //Mengeksekusi query 
    $simpan_layanan=mysqli_query($kon,$sql);

    $id_pengguna=$_SESSION["id_pengguna"];
    $waktu=date("Y-m-d H:i:s");
    $log_aktivitas="Tambah Jenis Layanan #$kode_jenis_layanan ";
    $simpan_aktivitas=mysqli_query($kon,"insert into log_aktivitas (waktu,aktivitas,id_pengguna) values ('$waktu','$log_aktivitas',$id_pengguna)");

    //Kondisi apakah berhasil atau tidak dalam mengeksekusi query diatas
    if ($simpan_layanan and $simpan_aktivitas) {
        //jka berhasil lakukan commit
        mysqli_query($kon,"COMMIT");
    }
    else {
        //jika gagal kembalikan atau rollback
        mysqli_query($kon,"ROLLBACK");
    }

          
?>