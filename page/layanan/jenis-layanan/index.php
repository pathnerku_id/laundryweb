<?php
  session_start();
  if (isset($_POST["id_layanan"])) {
    $_SESSION["id_layanan"]=$_POST["id_layanan"];
  }
?>

<!-- Tombol tambah Jenis layanan -->
<button id_layanan="<?php echo $id_layanan; ?>"  id="btn_tambah_jenis_layanan" type="button" class="btn btn-primary"  data-toggle="collapse" data-target="#demo"><span class="text">Tambah</span></button>
<hr>
<div id="demo" class="collapse">
  <?php
    // mengambil data layanan dengan kode paling besar
    include '../../../config/database.php';
    $query = mysqli_query($kon, "SELECT max(id_layanan) as kodeTerbesar FROM jenis_layanan");
    $data = mysqli_fetch_array($query);
    $id_layanan = $data['kodeTerbesar'];
    $id_layanan++;
    $huruf = "J";
    $kode_jenis_layanan = $huruf . sprintf("%04s", $id_layanan);
  ?>
  <div class="card mb-4">
      <div class="card-body">
          <form method="post" id="form_jenis_layanan">
                <!-- rows -->
                <div class="row">
                  <div class="col-sm-12">
                      <div class="form-group">
                          <input name="kode_jenis_layanan" value="<?php echo $kode_jenis_layanan; ?>" type="hidden" class="form-control">
                          <input name="id_layanan" value="<?php echo $_SESSION["id_layanan"]; ?>" type="hidden" class="form-control">
                      </div>
                  </div>
              </div>
              <!-- rows -->
              <div class="row">
                  <div class="col-sm-12">
                      <div class="form-group">
                          <label>Nama jenis layanan:</label>
                          <input name="nama_jenis_layanan" type="text" class="form-control" placeholder="Masukan Nama Jenis layanan" required>
                      </div>
                  </div>
              </div>
              <!-- rows -->
              <div class="row">
                  <div class="col-sm-6">
                      <div class="form-group">
                          <label>Estimasi Waktu:</label>
                          <input name="estimasi_waktu" id ="estimasi_waktu" type="text" class="form-control" placeholder="Masukan Nama Estimasi Waktu" required>
                      </div>
                      <div class="form-group">
                          <div id="info_estimasi_waktu" class='font-weight-bold'></div>
                      </div>
                  </div>
                  <div class="col-sm-6">
                      <div class="form-group">
                          <label>Tarif/ KG:</label>
                          <input name="tarif"  id="tarif" type="text" class="form-control"  placeholder="Masukan Tarif" required>
                      </div>
                      <div class="form-group">
                          <div id="info_tarif" class='font-weight-bold'></div>
                      </div>
                  </div>
              </div>
              <button type="button"  id="submit_form_jenis_layanan"   class="btn btn-dark">Tambah</button>
          </form>
      </div>
  </div>
</div>

<div class="table-responsive">
  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
      <tr>
        <th>No</th>
        <th>Jenis layanan</th>
        <th>Estimasi Waktu</th>
        <th>Tarif</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
        <?php
            // include database
            include '../../../config/database.php';
            $id_layanan= $_SESSION["id_layanan"];
            // perintah sql untuk menampilkan jenis layanan
            $sql="select * from jenis_layanan  where id_layanan='$id_layanan' order by id_jenis_layanan desc";
            $hasil=mysqli_query($kon,$sql);
            $no=0;
            //Menampilkan data dengan perulangan while
            while ($data = mysqli_fetch_array($hasil)):
            $no++;
        ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $data['nama_jenis_layanan']; ?></td>
            <td><?php echo $data['estimasi_waktu']; ?> Hari</td>
            <td>Rp. <?php echo number_format($data['tarif'],0,',','.'); ?></td>
            <td>
                <button class="hapus_jenis_layanan btn btn-danger btn-circle"  id_jenis_layanan="<?php echo $data['id_jenis_layanan']; ?>"  data-toggle="tooltip" title="Hapus pengguna" data-placement="top"><i class="fas fa-trash"></i></button>
            </td>
        </tr>
        <!-- bagian akhir (penutup) while -->
        <?php endwhile; ?>
    </tbody>
  </table>
</div>

<!-- Modal -->
<div class="modal fade" id="tambahJenislayanan">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Bagian header -->
      <div class="modal-header">
        <h4 class="modal-title" id="judul2"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Bagian body -->
      <div class="modal-body">
        <div id="tampil_modal_jenis_layanan">
            <!-- Menampilkan data dengan ajax -->
        </div>   
      </div>
      <!-- Bagian footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<script>
  //Tombol tambah jenis layanan akan di sembunyukan saat setelah diklik
  $("#btn_tambah_jenis_layanan").click(function(){
    $('#btn_tambah_jenis_layanan').hide(200);
  });

  //Menambah jenis layanan
  $("#submit_form_jenis_layanan").click(function(){
      var data = $('#form_jenis_layanan').serialize();
      $.ajax({
          type	: 'POST',
          url	: "page/layanan/jenis-layanan/tambah-jenis-layanan.php",
          data: data,
          cache	: false,
          success	: function(data){
         
            $('#tampil_jenis_layanan').load("page/layanan/jenis-layanan/index.php");
          }
      });
  });

  // fungsi hapus jenis layanan
  $('.hapus_jenis_layanan').on('click',function(){

    var id_jenis_layanan = $(this).attr("id_jenis_layanan");
    konfirmasi=confirm("Menghapus jenis layanan dapat mempengaruhi data transaksi yang telah dilakukan. Yakin ingin menghapus?")

    if (konfirmasi){
        $.ajax({
        url: 'page/layanan/jenis-layanan/hapus-jenis-layanan.php',
        method: 'post',
        data: {id_jenis_layanan:id_jenis_layanan},
        success:function(data){
          $('#tampil_jenis_layanan').load("page/layanan/jenis-layanan/index.php"); 
        }
        });
    }
  });

  $('#estimasi_waktu').bind('keyup', function () {
        var estimasi_waktu=$("#estimasi_waktu").val();
        $("#info_estimasi_waktu").text(estimasi_waktu+' Hari');     
  });


  $('#tarif').bind('keyup', function () {
        var tarif=$("#tarif").val();
        $("#info_tarif").text('Rp.'+format_rupiah(tarif));     
  });


  function format_rupiah(nominal){
      var  reverse = nominal.toString().split('').reverse().join(''),
          ribuan = reverse.match(/\d{1,3}/g);
      return ribuan	= ribuan.join('.').split('').reverse().join('');
    }

</script>