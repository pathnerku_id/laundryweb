<?php
    session_start();
    include '../../../config/database.php';
    //Memulai transaksi
    mysqli_query($kon,"START TRANSACTION");
    $id_jenis_layanan=$_POST["id_jenis_layanan"];

    $sql="delete from jenis_layanan where id_jenis_layanan=$id_jenis_layanan";
    $hapus_jenis_layanan=mysqli_query($kon,$sql);

    $id_pengguna=$_SESSION["id_pengguna"];
    $waktu=date("Y-m-d H:i:s");
    $log_aktivitas="Hapus Jenis Layanan ID #$id_jenis_layanan ";
    $simpan_aktivitas=mysqli_query($kon,"insert into log_aktivitas (waktu,aktivitas,id_pengguna) values ('$waktu','$log_aktivitas',$id_pengguna)");

    //Kondisi apakah berhasil atau tidak dalam mengeksekusi query diatas
    if ($hapus_jenis_layanan and $simpan_aktivitas) {
        //jka berhasil lakukan commit
        mysqli_query($kon,"COMMIT");
    }
    else {
        //jika gagal kembalikan atau rollback
        mysqli_query($kon,"ROLLBACK");
    }
?>