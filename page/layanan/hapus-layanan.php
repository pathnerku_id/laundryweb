<?php
    //Memulai sessi
    session_start();
    include '../../config/database.php';

    $id_layanan=$_POST["id_layanan"];
    $gambar=$_POST["gambar"];
    
    //Perintah sql untuk menghapus layanan
    $sql="delete from layanan where id_layanan=$id_layanan";
    $hapus_layanan=mysqli_query($kon,$sql);

    //Menghapus file gambar selain gambar default
    if ($gambar!='gambar_default.png'){
        unlink("gambar/".$gambar);
    }

    $id_pengguna=$_SESSION['id_pengguna'];
    $waktu=date("Y-m-d H:i:s");
    $log_aktivitas="Hapus layanan ID #$id_layanan ";
    $simpan_aktivitas=mysqli_query($kon,"insert into log_aktivitas (waktu,aktivitas,id_pengguna) values ('$waktu','$log_aktivitas',$id_pengguna)");

    //Kondisi apakah berhasil atau tidak dalam mengeksekusi query diatas
    if ($hapus_layanan and $simpan_aktivitas) {
        mysqli_query($kon,"COMMIT");
    }
    else {
        mysqli_query($kon,"ROLLBACK");
    }

?>