<?php
session_start();
    if (isset($_POST['tambah_layanan'])) {
        //Include file koneksi, untuk koneksikan ke database
        include '../../config/database.php';
        
        //Fungsi untuk mencegah inputan karakter yang tidak sesuai
        function input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
        //Cek apakah ada kiriman form dari method post
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            mysqli_query($kon,"START TRANSACTION");

            $kode_layanan=input($_POST["kode_layanan"]);
            $nama_layanan=input($_POST["nama_layanan"]);

            $ekstensi_diperbolehkan	= array('png','jpg','gif','jpeg');
            $gambar_layanan = $_FILES['gambar_layanan']['name'];
            $x = explode('.', $gambar_layanan);
            $ekstensi = strtolower(end($x));
            $ukuran	= $_FILES['gambar_layanan']['size'];
            $file_tmp = $_FILES['gambar_layanan']['tmp_name'];	

            if (!empty($gambar_layanan)){
                if(in_array($ekstensi, $ekstensi_diperbolehkan) === true){

                    //Mengupload gambar
                    move_uploaded_file($file_tmp, 'gambar/'.$gambar_layanan);

                    $sql="insert into layanan (kode_layanan,nama_layanan,gambar_layanan) values
                    ('$kode_layanan','$nama_layanan','$gambar_layanan')";
                }
            }else {
                $sql="insert into layanan (kode_layanan,nama_layanan,gambar_layanan) values
                ('$kode_layanan','$nama_layanan','gambar_default.png')";
            }

            //Mengeksekusi/menjalankan query 
            $simpan_layanan=mysqli_query($kon,$sql);

            $id_pengguna=$_SESSION['id_pengguna'];
            $waktu=date("Y-m-d H:i:s");
            $log_aktivitas="Tambah layanan #$kode_layanan ";
            $simpan_aktivitas=mysqli_query($kon,"insert into log_aktivitas (waktu,aktivitas,id_pengguna) values ('$waktu','$log_aktivitas',$id_pengguna)");

            //Kondisi apakah berhasil atau tidak dalam mengeksekusi query diatas
            if ($simpan_layanan and $simpan_aktivitas) {
                mysqli_query($kon,"COMMIT");
                header("Location:../../index.php?page=layanan&tambah=berhasil");
            }
            else {
                mysqli_query($kon,"ROLLBACK");
                header("Location:../../index.php?page=layanan&tambah=gagal");
            }
        }
    }

    // mengambil data layanan dengan kode paling besar
    include '../../config/database.php';
    $query = mysqli_query($kon, "SELECT max(id_layanan) as kodeTerbesar FROM layanan");
    $data = mysqli_fetch_array($query);
    $id_layanan = $data['kodeTerbesar'];
    $id_layanan++;
    $huruf = "P";
    $kodelayanan = $huruf . sprintf("%04s", $id_layanan);

?>
<form action="page/layanan/tambah-layanan.php" method="post" enctype="multipart/form-data">
    <!-- rows -->
    <div class="row">
        <div class="col-sm-10">
            <div class="form-group">
                <label>Nama layanan:</label>
                <input name="nama_layanan" type="text" class="form-control" placeholder="Masukan Nama layanan" required>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label>Kode layanan:</label>
                <h3><?php echo $kodelayanan; ?></h3>
                <input name="kode_layanan" value="<?php echo $kodelayanan; ?>" type="hidden" class="form-control">
            </div>
        </div>
    </div>
    <!-- rows -->   
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <div id="msg"></div>
                <label>Gambar layanan:</label>
                <input type="file" name="gambar_layanan" class="file" >
                    <div class="input-group my-3">
                        <input type="text" class="form-control" disabled placeholder="Upload Gambar" id="file">
                        <div class="input-group-append">
                                <button type="button" id="pilih_logo" class="browse btn btn-dark">Pilih Logo</button>
                        </div>
                    </div>
                <img src="assets/img/img80.png" id="preview" class="img-thumbnail">
            </div>
        </div>
    </div>
    <button type="submit" name="tambah_layanan" class="btn btn-dark">Tambah</button>
</form>
<style>
    .file {
    visibility: hidden;
    position: absolute;
    }
</style>
<script>
    $(document).on("click", "#pilih_logo", function() {
    var file = $(this).parents().find(".file");
    file.trigger("click");
    });
    $('input[type="file"]').change(function(e) {
    var fileName = e.target.files[0].name;
    $("#file").val(fileName);

    var reader = new FileReader();
    reader.onload = function(e) {
        document.getElementById("preview").src = e.target.result;
    };
    reader.readAsDataURL(this.files[0]);
    });
</script>
