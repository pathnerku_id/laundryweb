<?php
    //Mengambil id_pengguna dalam variabel session
    $id_pengguna= $_SESSION["id_pengguna"];

    //Cek jika level bukan Super Admin maka tidak punya hak akses
    if ($_SESSION["level"]!="Super Admin"):
        echo"<div class='alert alert-danger'>Anda tidak punya hak akses</div>";
        exit;
    endif;
?>
<script>
    $('title').text('Jenis Layanan Laundry');
</script>
<div class="container-fluid">
<!--Bagian heading -->
<h1 class="h3 mb-2 text-gray-800">Layanan</h1>
<p class="mb-4">Halaman layanan berisi informasi seluruh layanan yang dapat di kelolah oleh admin.</p>
<?php
    //Validasi untuk menampilkan pesan pemberitahuan saat user menambah layanan
    if (isset($_GET['tambah'])) {
        if ($_GET['tambah']=='berhasil'){
            echo"<div class='alert alert-success'><strong>Berhasil!</strong> layanan telah ditambah</div>";
        }else if ($_GET['tambah']=='gagal'){
            echo"<div class='alert alert-danger'><strong>Gagal!</strong> layanan gagal ditambahkan</div>";
        }    
    }

    //Validasi untuk menampilkan pesan pemberitahuan saat user menghapus layanan
    if (isset($_GET['hapus'])) {
      if ($_GET['hapus']=='berhasil'){
          echo"<div class='alert alert-success'><strong>Berhasil!</strong> layanan telah dihapus</div>";
      }else if ($_GET['hapus']=='gagal'){
          echo"<div class='alert alert-danger'><strong>Gagal!</strong> layanan Pagal dihapus</div>";
      }    
    }
?>


<!-- Modal -->
<div class="modal fade" id="tambahlayanan">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Bagian header -->
      <div class="modal-header">
        <h4 class="modal-title" id="judul"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Bagian body -->
      <div class="modal-body">
        
        <div id="tampil_data">
          <!--Data akan ditampilkan disini dengan ajax -->
        </div>
            
      </div>
      <!-- Bagian footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<div class="card shadow mb-4">
  <div class="card-header py-3">
    <!-- Tombol tambah layanan -->
    <button class="btn-tambah btn btn-dark btn-icon-split" data-toggle="modal" data-target="#tambahlayanan"><span class="text">Tambah</span></button>
  </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-6">
          <div class="card-columns">
            <?php         
              // include database
              include 'config/database.php';
              // perintah sql 
              $sql="select * from layanan";
              $hasil=mysqli_query($kon,$sql);
              $no=0;
              //Menampilkan data dengan perulangan while
              while ($data = mysqli_fetch_array($hasil)):
              $no++;
            ?>
              <div class="card bg-basic" style="width: 12rem;">
              <a href="#" class="hapus_layanan" id_layanan="<?php echo $data['id_layanan'];?>"  gambar="<?php echo $data['gambar_layanan']; ?>"><h6 class="text-danger text-center"><small>Hapus</small></h6></a>
              <img class="card-img-top" src="page/layanan/gambar/<?php echo $data['gambar_layanan'];?>" alt="Card image cap">
                <div class="card-body text-center">
                <button type="button" id_layanan="<?php echo $data['id_layanan'];?>" nama_layanan="<?php echo $data['nama_layanan'];?>"  class="layanan btn btn-light"><?php echo $data['nama_layanan'];?></button>
              </div>
              </div>
            <?php endwhile; ?>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card mb-4">
              <div class="card-header" id="judul_2">Layanan</div>
                <div class="card-body" id="tampil_jenis_layanan">
                <!-- Jenis layanan ditampilkan disini menggunakan AJAX -->
                </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>

<script>

  // saat user mengklik salah satu layanan mana jenis layanan akan ditampilkan
  $('.layanan').on('click',function(){

      var id_layanan = $(this).attr("id_layanan");
      var nama_layanan = $(this).attr("nama_layanan");
      $.ajax({
          url: 'page/layanan/jenis-layanan/index.php',
          method: 'post',
          data: {id_layanan:id_layanan},
          success:function(data){
              $('#tampil_jenis_layanan').html(data);  
              document.getElementById("judul_2").innerHTML='layanan : '+nama_layanan;
          }
      });
  });

  // tambah layanan
  $('.btn-tambah').on('click',function(){
      $.ajax({
          url: 'page/layanan/tambah-layanan.php',
          method: 'post',
          success:function(data){
              $('#tampil_data').html(data);  
              document.getElementById("judul").innerHTML='Tambah layanan';
          }
      });
      // Membuka modal
      $('#modal').modal('show');
  });


  // fungsi layanan
  $('.hapus_layanan').on('click',function(){
    var id_layanan = $(this).attr("id_layanan");
    var gambar = $(this).attr("gambar");

    konfirmasi=confirm("Menghapus layanan dapat mempengaruhi data transaksi yang telah dilakukan. Yakin ingin menghapus?")

    if (konfirmasi){
        $.ajax({
          url: 'page/layanan/hapus-layanan.php',
            method: 'post',
            data: {id_layanan:id_layanan,gambar:gambar},
            success:function(data){
                window.location.href = 'index.php?page=layanan&hapus=berhasil';
            }
        });
    }
  });
</script>

