<div class="card shadow mb-4">
	<div class="card-body">
	  <!-- Tabel daftar transaksi -->
	  <div class="table-responsive">
		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
		  <thead>
			<tr>
			  <th>No</th>
			  <th>Kode</th>
			  <th>Nama</th>
			  <th>Jumlah Transaksi</th>
			  <th>Total Pendapatan</th>
			</tr>
		  </thead>
		  <tbody>
			<?php
				// include database
				include '../../../config/database.php';
				$kondisi="";

				if (!empty($_POST["dari_tanggal"]) && empty($_POST["sampai_tanggal"])) $kondisi= "where date(tanggal_transaksi)='".$_POST['dari_tanggal']."' ";
				if (!empty($_POST["dari_tanggal"]) && !empty($_POST["sampai_tanggal"])) $kondisi= "where date(tanggal_transaksi) between '".$_POST['dari_tanggal']."' and '".$_POST['sampai_tanggal']."'";
				
				$sql="SELECT p.kode_pengguna as kode_admin, p.nama_pengguna as nama_admin,count(t.id_transaksi) as jumlah_transaksi,sum(total_biaya) as total_pendapatan
				FROM transaksi t INNER JOIN pengguna p on t.id_pengguna=p.id_pengguna
				$kondisi 
				GROUP BY kode_admin,nama_admin
				ORDER BY kode_admin asc";
			
				// perintah sql
				$hasil=mysqli_query($kon,$sql);
				$no=0;
				$total_tranksaksi=0;
				$total_pendapatan=0;
				//Menampilkan data dengan perulangan while
				while ($data = mysqli_fetch_array($hasil)):
				$no++;
				$total_tranksaksi+=$data['jumlah_transaksi'];
				$total_pendapatan+=$data['total_pendapatan'];
			?>
			  <tr>
				  <td><?php echo $no; ?></td>
				  <td><?php echo $data['kode_admin']; ?></td>
				  <td><?php echo $data['nama_admin']; ?></td>
				  <td><?php echo $data['jumlah_transaksi']; ?></td>
				  <td>Rp. <?php echo number_format($data['total_pendapatan'],0,',','.'); ?></td>
			
			  </tr>
			  <!-- bagian akhir (penutup) while -->
			  <?php endwhile; ?>
			  <tr><td colspan="3"><strong>Total</strong></td><td><strong><?php echo $total_tranksaksi; ?></strong></td><td><strong>Rp. <?php echo number_format($total_pendapatan,0,',','.'); ?></strong></td> </tr>
	
		  </tbody>
		</table>
	  </div>
	  <a href="page/laporan/per-admin/cetak/cetak-laporan.php?dari_tanggal=<?php if (!empty($_POST["dari_tanggal"])) echo $_POST["dari_tanggal"]; ?>&sampai_tanggal=<?php if (!empty($_POST["sampai_tanggal"])) echo $_POST["sampai_tanggal"]; ?>" target='blank' class="btn btn-primary btn-icon-split"><span class="text"><i class="fas fa-print fa-sm"></i> Cetak</span></a>
      <a href="page/laporan/per-admin/cetak/cetak-laporan-pdf.php?dari_tanggal=<?php if (!empty($_POST["dari_tanggal"])) echo $_POST["dari_tanggal"]; ?>&sampai_tanggal=<?php if (!empty($_POST["sampai_tanggal"])) echo $_POST["sampai_tanggal"]; ?>" target='blank' class="btn btn-danger btn-icon-pdf"><span class="text"><i class="fas fa-file-pdf fa-sm"></i> Export PDF</span></a>
	  <a href="page/laporan/per-admin/cetak/cetak-laporan-excel.php?dari_tanggal=<?php if (!empty($_POST["dari_tanggal"])) echo $_POST["dari_tanggal"]; ?>&sampai_tanggal=<?php if (!empty($_POST["sampai_tanggal"])) echo $_POST["sampai_tanggal"]; ?>" target='blank' class="btn btn-success btn-icon-pdf"><span class="text"><i class="fas fa-file-excel fa-sm"></i> Export Excel</span></a>
	</div>
</div>