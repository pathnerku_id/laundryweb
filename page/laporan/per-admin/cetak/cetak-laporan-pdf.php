<?php
    //Mengambil plugin fpdf
    require('../../../../assets/vendor/fpdf/fpdf.php');
    $pdf = new FPDF('P', 'mm','Letter');

    //Mengambil profil aplikasi
    include '../../../../config/database.php';
    $query = mysqli_query($kon, "select * from profil_aplikasi order by nama_aplikasi desc limit 1");    
    $row = mysqli_fetch_array($query);

    //Membuat halaman pdf
    $pdf->AddPage();
    $pdf->Image('../../../../page/aplikasi/logo/'.$row['logo'],15,5,30,30);
    $pdf->SetFont('Arial','B',21);
    $pdf->Cell(0,7,strtoupper($row['nama_aplikasi']),0,1,'C');
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(0,7,$row['alamat'].', Telp '.$row['no_telp'],0,1,'C');
    $pdf->Cell(0,7,$row['website'],0,1,'C');
    $pdf->Cell(10,7,'',0,1);
    $tanggal='';
    
    //Menampilkan rentan tanggal
    if (!empty($_GET["dari_tanggal"]) && empty($_GET["sampai_tanggal"])){
        $tanggal=date("d/m/Y",strtotime($_GET["dari_tanggal"]));
    }
    if (!empty($_GET["dari_tanggal"]) && !empty($_GET["sampai_tanggal"])){
        $tanggal=date("d/m/Y",strtotime($_GET["dari_tanggal"]))." - ".date("d/m/Y",strtotime($_GET["sampai_tanggal"]));
    }

    $pdf->SetFont('Arial','',11);
    $pdf->Cell(55,6,'Laporan Pendapatan Tanggal: ',0,0);
    $pdf->Cell(30,6,$tanggal,0,1);
    //Membuat header tabel
    $pdf->Cell(10,3,'',0,1);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(8,6,'No',1,0,'C');
    $pdf->Cell(15,6,'Kode',1,0,'C');
    $pdf->Cell(70,6,'Nama',1,0,'C');
    $pdf->Cell(45,6,'Jumlah Transaksi',1,0,'C');
    $pdf->Cell(40,6,'Pendapatan',1,1,'C');

    $pdf->SetFont('Arial','',10);
    //Kondisi berdasarkan rentan tanggal
    $kondisi="";
    if (!empty($_GET["dari_tanggal"]) && empty($_GET["sampai_tanggal"])) $kondisi= "where date(tanggal_transaksi)='".$_GET['dari_tanggal']."' ";
    if (!empty($_GET["dari_tanggal"]) && !empty($_GET["sampai_tanggal"])) $kondisi= "where date(tanggal_transaksi) between '".$_GET['dari_tanggal']."' and '".$_GET['sampai_tanggal']."'";
    
    $no=1;
	$total_tranksaksi=0;
	$total_pendapatan=0;
    //Mengambil data dari database
    $hasil = mysqli_query($kon, "SELECT p.kode_pengguna as kode_admin, p.nama_pengguna as nama_admin,count(t.id_transaksi) as jumlah_transaksi,sum(total_biaya) as total_pendapatan FROM transaksi t INNER JOIN pengguna p on t.id_pengguna=p.id_pengguna $kondisi GROUP BY kode_admin,nama_admin ORDER BY kode_admin asc");
    while ($data = mysqli_fetch_array($hasil)){
      
        $total_tranksaksi+=$data['jumlah_transaksi'];
        $total_pendapatan+=$data['total_pendapatan'];
        //Menampilkan data
        $pdf->Cell(8,6,$no,1,0);
        $pdf->Cell(15,6,$data['kode_admin'],1,0,'C');
        $pdf->Cell(70,6,$data['nama_admin'],1,0);
        $pdf->Cell(45,6,$data['jumlah_transaksi'],1,0,'C');
        $pdf->Cell(40,6,'Rp. '.number_format($data['total_pendapatan'],0,',','.'),1,1,'C');
        $no++;
    }
    //Menampilkan total
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(93,6,'Total',1,0,'C');
    $pdf->Cell(45,6,number_format($total_tranksaksi,0,',','.'),1,0,'C');
    $pdf->Cell(40,6,'Rp. '.number_format($total_pendapatan,0,',','.'),1,1,'C');
    $pdf->Output();
?>