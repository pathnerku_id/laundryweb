<!DOCTYPE html>
<html>
<head>
  <!-- Mengambil css templates -->
  <link href="../../../../assets/css/sb-admin-2.min.css" rel="stylesheet">
</head>
    <body onload="window.print();">
        <?php
        include '../../../../config/database.php';
        $query = mysqli_query($kon, "select * from profil_aplikasi order by nama_aplikasi desc limit 1");    
        $row = mysqli_fetch_array($query);
        ?>
        <div class="container-fluid">
            <div class="card">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-sm-2 float-left">
                    <img src="../../../../page/aplikasi/logo/<?php echo $row['logo']; ?>" width="95px" alt="brand"/>
                    </div>
                    <div class="col-sm-10 float-left">
                        <h3><?php echo strtoupper($row['nama_aplikasi']);?></h3>
                        <h6><?php echo $row['alamat'].', Telp '.$row['no_telp'];?></h6>
                        <h6><?php echo $row['website'];?></h6>
                    </div>
                </div>
            </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Jumlah Transaksi</th>
                                    <th>Total Pendapatan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        // include database
                                        include '../../../../config/database.php';
                                        $kondisi="";

                                        if (!empty($_GET["dari_tanggal"]) && empty($_GET["sampai_tanggal"])) $kondisi= "where date(tanggal_transaksi)='".$_GET['dari_tanggal']."' ";
                                        if (!empty($_GET["dari_tanggal"]) && !empty($_GET["sampai_tanggal"])) $kondisi= "where date(tanggal_transaksi) between '".$_GET['dari_tanggal']."' and '".$_GET['sampai_tanggal']."'";
                                        
                                        $sql="SELECT p.kode_pengguna as kode_admin, p.nama_pengguna as nama_admin,count(t.id_transaksi) as jumlah_transaksi,sum(total_biaya) as total_pendapatan
                                        FROM transaksi t INNER JOIN pengguna p on t.id_pengguna=p.id_pengguna
                                        $kondisi 
                                        GROUP BY kode_admin,nama_admin
                                        ORDER BY kode_admin asc";
                                    
                                        $hasil=mysqli_query($kon,$sql);
                                        $no=0;
                                        $total_tranksaksi=0;
                                        $total_pendapatan=0;
                                        //Menampilkan data dengan perulangan while
                                        while ($data = mysqli_fetch_array($hasil)):
                                        $no++;
                                        $total_tranksaksi+=$data['jumlah_transaksi'];
                                        $total_pendapatan+=$data['total_pendapatan'];
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $data['kode_admin']; ?></td>
                                        <td><?php echo $data['nama_admin']; ?></td>
                                        <td><?php echo $data['jumlah_transaksi']; ?></td>
                                        <td>Rp. <?php echo number_format($data['total_pendapatan'],0,',','.'); ?></td>
                                    
                                    </tr>
                                    <!-- bagian akhir (penutup) while -->
                                    <?php endwhile; ?>
                                    <tr><td colspan="3"><strong>Total</strong></td><td><strong><?php echo $total_tranksaksi; ?></strong></td><td><strong>Rp. <?php echo number_format($total_pendapatan,0,',','.'); ?></strong></td> </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>