
<script>
    $('title').text('Laporan Pendapatan Laundry');
</script>
<div class="container-fluid">
<!--Bagian heading -->
<h1 class="h3 mb-2 text-gray-800">Laporan Pendapatan Berdasarkan <?php echo ucfirst($_GET['isi']);?></h1>
<p class="mb-4">Halaman laporan berisi informasi seluruh laporan yang dapat di kelolah oleh admin.</p>

<input type="hidden" name="isi_laporan" id="isi_laporan" value="<?php echo $_GET['isi'];?>" ?>

<div class="card shadow mb-4">
  <div class="card-body">
  <div id="filter_laporan" class="collapse show">
      <!-- form -->
      <form method="post" id="form">
        <div class="form-row">
            <div class="col-sm-3">
              <input type="date" class="form-control" name="dari_tanggal" required>
            </div>
            <div class="col-sm-3">
              <input type="date" class="form-control"  name="sampai_tanggal" required>
            </div>
            <div class="col-sm-3">
            <button  type="button" id="btn-tampil"  class="btn btn-dark"><span class="text"><i class="fas fa-search fa-sm"></i> Tampilkan</span></button>
            </div>
        </div>
      </form>
   </div>
    </div>

  </div>
  <!-- Tampil Laporan -->
    <div id="tampil_laporan">
      
    </div>
    <div id='ajax-wait'>
            <img alt='loading...' src='assets/img/Rolling-1s-84px.png' />
    </div>

    <style>
        #ajax-wait {
            display: none;
            position: fixed;
            z-index: 1999
        }
    </style>
</div>


<script type="text/javascript">

  
  $('#btn-tampil').on('click',function(){

    loading();
    var isi_laporan = $('#isi_laporan').val();

    switch(isi_laporan){
      case 'transaksi':  laporan_per_transaksi()
      break;
      case 'admin':  laporan_per_admin()
      break;
    }


  });


  function loading(){
    $( document ).ajaxStart(function() {
      $( "#ajax-wait" ).css({
          left: ( $( window ).width() - 32 ) / 2 + "px", // 32 = lebar gambar
          top: ( $( window ).height() - 32 ) / 2 + "px", // 32 = tinggi gambar
          display: "block"
      })
      })
      .ajaxComplete( function() {
          $( "#ajax-wait" ).fadeOut();
    });
  }

  function laporan_per_transaksi(){
    var data = $('#form').serialize();
      $.ajax({
        type	: 'POST',
        url	: "page/laporan/per-transaksi/laporan-per-transaksi.php",
        data: data,
        cache	: false,
        success	: function(data){
          $("#tampil_laporan").html(data);

        }
      });
  }


  function laporan_per_admin(){
    var data = $('#form').serialize();
      $.ajax({
        type	: 'POST',
        url	: "page/laporan/per-admin/laporan-per-admin.php",
        data: data,
        cache	: false,
        success	: function(data){
          $("#tampil_laporan").html(data);

        }
      });
  }

</script>