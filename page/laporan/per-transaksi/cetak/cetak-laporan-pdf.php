<?php
    //Mengambil plugin fpdf
    require('../../../../assets/vendor/fpdf/fpdf.php');
    $pdf = new FPDF('P', 'mm','Letter');

    //Mengambil profil aplikasi
    include '../../../../config/database.php';
    $query = mysqli_query($kon, "select * from profil_aplikasi order by nama_aplikasi desc limit 1");    
    $row = mysqli_fetch_array($query);

    //Membuat halaman pdf
    $pdf->AddPage();
    $pdf->Image('../../../../page/aplikasi/logo/'.$row['logo'],15,5,30,30);
    $pdf->SetFont('Arial','B',21);
    $pdf->Cell(0,7,strtoupper($row['nama_aplikasi']),0,1,'C');
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(0,7,$row['alamat'].', Telp '.$row['no_telp'],0,1,'C');
    $pdf->Cell(0,7,$row['website'],0,1,'C');
    $pdf->Cell(10,7,'',0,1);
    $tanggal='';

    //Menampilkan rentan tanggal
    if (!empty($_GET["dari_tanggal"]) && empty($_GET["sampai_tanggal"])){
        $tanggal=date("d/m/Y",strtotime($_GET["dari_tanggal"]));
    }
    if (!empty($_GET["dari_tanggal"]) && !empty($_GET["sampai_tanggal"])){
        $tanggal=date("d/m/Y",strtotime($_GET["dari_tanggal"]))." - ".date("d/m/Y",strtotime($_GET["sampai_tanggal"]));
    }

    $pdf->SetFont('Arial','',11);
    $pdf->Cell(53,6,'Laporan Transaksi Tanggal: ',0,0);
    $pdf->Cell(30,6,$tanggal,0,1);

    //Membuat header tabel
    $pdf->Cell(10,3,'',0,1);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(8,6,'No',1,0,'C');
    $pdf->Cell(23,6,'Tanggal',1,0,'C');
    $pdf->Cell(20,6,'No Invoice',1,0,'C');
    $pdf->Cell(45,6,'Pelanggan',1,0,'C');
    $pdf->Cell(30,6,'layanan',1,0,'C');
    $pdf->Cell(25,6,'Jenis',1,0,'C');
    $pdf->Cell(13,6,'Berat',1,0,'C');
    $pdf->Cell(30,6,'Biaya',1,1,'C');

    
    $pdf->SetFont('Arial','',10);

    $kondisi="";

    if (!empty($_GET["dari_tanggal"]) && empty($_GET["sampai_tanggal"])) $kondisi= "where date(tanggal_transaksi)='".$_GET['dari_tanggal']."' ";
    if (!empty($_GET["dari_tanggal"]) && !empty($_GET["sampai_tanggal"])) $kondisi= "where date(tanggal_transaksi) between '".$_GET['dari_tanggal']."' and '".$_GET['sampai_tanggal']."'";
    
    $no=0;
    $biaya=0;
    $total_biaya=0;
    //Mengambil data dari database
    $hasil = mysqli_query($kon, "SELECT * from transaksi t inner join layanan p on p.id_layanan=t.id_layanan inner join jenis_layanan j on j.id_jenis_layanan=t.id_jenis_layanan  $kondisi order by t.tanggal_transaksi desc");
    while ($data = mysqli_fetch_array($hasil)){
        $no++;
        $biaya= $data['total_biaya'];
        $total_biaya+=$biaya;
        //Menampilkan data
        $pdf->Cell(8,6,$no,1,0);
        $pdf->Cell(23,6,date("d/m/Y",strtotime($data['tanggal_transaksi'])),1,0);
        $pdf->Cell(20,6,$data['no_invoice'],1,0);
        $pdf->Cell(45,6,$data['nama_pelanggan'],1,0);
        $pdf->Cell(30,6,$data['nama_layanan'],1,0);
        $pdf->Cell(25,6,$data['nama_jenis_layanan'],1,0);
        $pdf->Cell(13,6,$data['berat'],1,0);
        $pdf->Cell(30,6,'Rp. '.number_format($biaya,0,',','.'),1,1);
      
    }
    //Menampilkan total
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(164,6,'Total',1,0,'C');
    $pdf->Cell(30,6,'Rp.'.number_format($total_biaya,0,',','.'),1,1);
   
    $pdf->Output();
?>