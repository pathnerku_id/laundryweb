<!DOCTYPE html>
<html>
<head>
  <!-- Custom styles for this template -->
  <link href="../../../../assets/css/sb-admin-2.min.css" rel="stylesheet">
</head>
    <body onload="window.print();">
        <?php
        include '../../../../config/database.php';
   
        $query = mysqli_query($kon, "select * from profil_aplikasi order by nama_aplikasi desc limit 1");    
        $row = mysqli_fetch_array($query);
        ?>
        <div class="container-fluid">
            <div class="card">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-sm-2 float-left">
                    <img src="../../../../page/aplikasi/logo/<?php echo $row['logo']; ?>" width="95px" alt="brand"/>
                    </div>
                    <div class="col-sm-10 float-left">
                        <h3><?php echo strtoupper($row['nama_aplikasi']);?></h3>
                        <h6><?php echo $row['alamat'].', Telp '.$row['no_telp'];?></h6>
                        <h6><?php echo $row['website'];?></h6>
                    </div>
                </div>
            </div>
                <div class="card-body">
                    <!--rows -->
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>No Invoice</th>
                                    <th>Pelanggan</th>
                                    <th>layanan</th>
                                    <th>Jenis</th>
                                    <th>Berat</th>
                                    <th>Total Biaya</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $kondisi="";

                                        if (!empty($_GET["dari_tanggal"]) && empty($_GET["sampai_tanggal"])) $kondisi= "where date(tanggal_transaksi)='".$_GET['dari_tanggal']."' ";
                                        if (!empty($_GET["dari_tanggal"]) && !empty($_GET["sampai_tanggal"])) $kondisi= "where date(tanggal_transaksi) between '".$_GET['dari_tanggal']."' and '".$_GET['sampai_tanggal']."'";
                                        
                                        $sql="SELECT * from transaksi t inner join layanan p on p.id_layanan=t.id_layanan inner join jenis_layanan j on j.id_jenis_layanan=t.id_jenis_layanan  $kondisi order by t.tanggal_transaksi desc";
                                    
                                        // perintah sql
                                        $hasil=mysqli_query($kon,$sql);
                                        $no=0;

                                        $total_biaya=0;
                                        //Menampilkan data dengan perulangan while
                                        while ($data = mysqli_fetch_array($hasil)):
                                        $no++;
                                        $biaya= $data['total_biaya'];
                                        $total_biaya+=$biaya;
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo date('d/m/Y', strtotime($data["tanggal_transaksi"]));?></td>
                                        <td><?php echo $data['no_invoice']; ?></td>
                                        <td><?php echo $data['nama_pelanggan']; ?></td>
                                        <td><?php echo $data['nama_layanan']; ?></td>
                                        <td><?php echo $data['nama_jenis_layanan']; ?></td>
                                        <td><?php echo $data['berat'];;?></td>
                                        <td>Rp. <?php echo number_format($biaya,0,',','.'); ?></td>
                                
                                    </tr>
                                    <!-- bagian akhir (penutup) while -->
                                    <?php endwhile; ?>
                                    <tr><td colspan="7"><strong>Total</strong></td><td><strong>Rp. <?php echo number_format($total_biaya,0,',','.'); ?></strong></td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>