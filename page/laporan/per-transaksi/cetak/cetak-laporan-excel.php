<?php
    //Koneksi database
    include '../../../../config/database.php';
    //Mengambil nama aplikasi
    $query = mysqli_query($kon, "select nama_aplikasi from profil_aplikasi order by nama_aplikasi desc limit 1");    
    $row = mysqli_fetch_array($query);

    //Mengambil tanggal
    $tanggal='';
    if (!empty($_GET["dari_tanggal"]) && empty($_GET["sampai_tanggal"])) $tanggal=date("d/m/Y",strtotime($_GET["dari_tanggal"]));
    if (!empty($_GET["dari_tanggal"]) && !empty($_GET["sampai_tanggal"])) $tanggal= "".date("d/m/Y",strtotime($_GET["dari_tanggal"]))." - ".date("d/m/Y",strtotime($_GET["sampai_tanggal"]))."";
    
    //Membuat file format excel
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=LAPORAN PENDAPATAN ".strtoupper($row['nama_aplikasi'])." PER TRANSAKSI ".$tanggal.".xls");
?>  
<h3><center>LAPORAN PENDAPATAN <?php echo strtoupper($row['nama_aplikasi']);?> PER TRANSAKSI </center></h3>
<h4>Tanggal : <?php echo $tanggal; ?></h4>

<table border="1">
    <thead>
    <tr>
        <th>No</th>
        <th>Tanggal</th>
        <th>No Invoice</th>
        <th>Pelanggan</th>
        <th>layanan</th>
        <th>Jenis</th>
        <th>Berat</th>
        <th>Total Biaya</th>
    </tr>
    </thead>
    <tbody>
    <?php
        // include database
        include '../../../../config/database.php';
        $kondisi="";
        //Kondisi berdasarkan rentan tanggal
        if (!empty($_GET["dari_tanggal"]) && empty($_GET["sampai_tanggal"])) $kondisi= "where date(tanggal_transaksi)='".$_GET['dari_tanggal']."' ";
        if (!empty($_GET["dari_tanggal"]) && !empty($_GET["sampai_tanggal"])) $kondisi= "where date(tanggal_transaksi) between '".$_GET['dari_tanggal']."' and '".$_GET['sampai_tanggal']."'";
        
        $sql="SELECT * from transaksi t inner join layanan p on p.id_layanan=t.id_layanan inner join jenis_layanan j on j.id_jenis_layanan=t.id_jenis_layanan  $kondisi order by t.tanggal_transaksi desc";
        $hasil=mysqli_query($kon,$sql);
        $no=0;
        $total_biaya=0;
        //Menampilkan data dengan perulangan while
        while ($data = mysqli_fetch_array($hasil)):
        $no++;
        $biaya= $data['total_biaya'];
        $total_biaya+=$biaya;
    ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo date('d/m/Y', strtotime($data["tanggal_transaksi"]));?></td>
            <td><?php echo $data['no_invoice']; ?></td>
            <td><?php echo $data['nama_pelanggan']; ?></td>
            <td><?php echo $data['nama_layanan']; ?></td>
            <td><?php echo $data['nama_jenis_layanan']; ?></td>
            <td><?php echo $data['berat'];;?></td>
            <td>Rp. <?php echo number_format($biaya,0,',','.'); ?></td>
        </tr>
        <!-- bagian akhir (penutup) while -->
        <?php endwhile; ?>
        <tr><td colspan="7"><strong>Total</strong></td><td><strong>Rp. <?php echo number_format($total_biaya,0,',','.'); ?></strong></td></tr>
    </tbody>
</table>
