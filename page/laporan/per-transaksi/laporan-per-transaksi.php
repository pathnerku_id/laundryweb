
<div class="card shadow mb-4">
	<div class="card-body">
	  <!-- Tabel daftar transaksi -->
	  <div class="table-responsive">
		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
		  <thead>
			<tr>
			  <th>No</th>
			  <th>Tanggal</th>
			  <th>No Invoice</th>
			  <th>Pelanggan</th>
			  <th>layanan</th>
			  <th>Jenis</th>
			  <th>Berat</th>
			  <th>Total Biaya</th>
			</tr>
		  </thead>
		  <tbody>
			<?php
				// include database
				include '../../../config/database.php';
				$kondisi="";
				//Kondisi berdasarkan rentan tanggal
				if (!empty($_POST["dari_tanggal"]) && empty($_POST["sampai_tanggal"])) $kondisi= "where date(tanggal_transaksi)='".$_POST['dari_tanggal']."' ";
				if (!empty($_POST["dari_tanggal"]) && !empty($_POST["sampai_tanggal"])) $kondisi= "where date(tanggal_transaksi) between '".$_POST['dari_tanggal']."' and '".$_POST['sampai_tanggal']."'";
				
				$sql="SELECT * from transaksi t inner join layanan p on p.id_layanan=t.id_layanan inner join jenis_layanan j on j.id_jenis_layanan=t.id_jenis_layanan  $kondisi order by t.tanggal_transaksi desc";
				$hasil=mysqli_query($kon,$sql);
				$no=0;

				$total_biaya=0;
				//Menampilkan data dengan perulangan while
				while ($data = mysqli_fetch_array($hasil)):
				$no++;
				$biaya= $data['total_biaya'];
				$total_biaya+=$biaya;
			?>
			  <tr>
				  <td><?php echo $no; ?></td>
				  <td><?php echo date('d/m/Y', strtotime($data["tanggal_transaksi"]));?></td>
				  <td><?php echo $data['no_invoice']; ?></td>
				  <td><?php echo $data['nama_pelanggan']; ?></td>
				  <td><?php echo $data['nama_layanan']; ?></td>
				  <td><?php echo $data['nama_jenis_layanan']; ?></td>
				  <td><?php echo $data['berat'];;?></td>
				  <td>Rp. <?php echo number_format($biaya,0,',','.'); ?></td>
			  </tr>
			  <!-- bagian akhir (penutup) while -->
			  <?php endwhile; ?>
			  <tr><td colspan="7"><strong>Total</strong></td><td><strong>Rp. <?php echo number_format($total_biaya,0,',','.'); ?></strong></td></tr>
		  </tbody>
		</table>
	  </div>
	  <a href="page/laporan/per-transaksi/cetak/cetak-laporan.php?dari_tanggal=<?php if (!empty($_POST["dari_tanggal"])) echo $_POST["dari_tanggal"]; ?>&sampai_tanggal=<?php if (!empty($_POST["sampai_tanggal"])) echo $_POST["sampai_tanggal"]; ?>" target='blank' class="btn btn-primary btn-icon-split"><span class="text"><i class="fas fa-print fa-sm"></i> Cetak</span></a>
      <a href="page/laporan/per-transaksi/cetak/cetak-laporan-pdf.php?dari_tanggal=<?php if (!empty($_POST["dari_tanggal"])) echo $_POST["dari_tanggal"]; ?>&sampai_tanggal=<?php if (!empty($_POST["sampai_tanggal"])) echo $_POST["sampai_tanggal"]; ?>" target='blank' class="btn btn-danger btn-icon-pdf"><span class="text"><i class="fas fa-file-pdf fa-sm"></i> Export PDF</span></a>
	  <a href="page/laporan/per-transaksi/cetak/cetak-laporan-excel.php?dari_tanggal=<?php if (!empty($_POST["dari_tanggal"])) echo $_POST["dari_tanggal"]; ?>&sampai_tanggal=<?php if (!empty($_POST["sampai_tanggal"])) echo $_POST["sampai_tanggal"]; ?>" target='blank' class="btn btn-success btn-icon-pdf"><span class="text"><i class="fas fa-file-excel fa-sm"></i> Export Excel</span></a>
	</div>
</div>