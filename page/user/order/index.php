<?php
 session_start();

 include '../../../config/database.php';
 if  (!isset($_SESSION["id_pelanggan"])){
   header("Location:../../../loginuser.php");
 }
 
 if(isset($_POST["pesan"])){
    var_dump($_POST);
    function input($data) {
        return $data;
    }
    function inputSelect($data,$index) {
        // $array = json_decode($data,true);
        $string = $data;
        // Menghapus tanda kurung siku pada awal dan akhir string
        $string = trim($string, "[]");

        // Mengganti tanda kutip tunggal dengan tanda kutip ganda
        $string = str_replace("'", '"', $string);

        // Mengubah string menjadi array dengan menggunakan fungsi explode
        $array = explode(",", $string);

        // Menghapus spasi pada setiap elemen array
        $array = array_map('trim', $array);
        return $array[$index];
    }
    function nextDay() {
        $jumlahHari = inputSelect($_POST['jenis_layanan'],3);
        $int = (int)$jumlahHari;
        $tanggalAwal = date('Y/m/d'); // Mendapatkan tanggal hari ini
        $tanggalAkhir = date('Y/m/d', strtotime($tanggalAwal . ' + ' . $int . ' days'));
        return $tanggalAkhir;
    }
    function totalBiaya() {
        $jumlahHari = inputSelect($_POST['jenis_layanan'],1);
        $berat = (int)$_POST['berat'];
        $ongkir = (int)$_POST['kecamatan'];
        $harga = (int)$jumlahHari;
        $total = ($harga * $berat) + $ongkir;
        return $total;
        
    }
    $no_invoice=input($_POST['no_invoice']);
    $id_pelanggan=input($_SESSION['id_pelanggan']);
    $nama_pelanggan=input($_POST['nama_pelanggan']);
    $no_telp=input($_POST['no_telp']);
    $alamat=input($_POST['alamat']);
    $id_pengguna=13;
    $id_layanan=inputSelect($_POST['layanan'],0);
    $id_jenis_layanan=inputSelect($_POST['jenis_layanan'],0);
    $berat=input($_POST['berat']);
    $tanggal_masuk = new DateTime();
    $tanggal_masuk = $tanggal_masuk->format('Y/m/d');
    $jam_sekarang=date("H:i");
    $tanggal_transaksi=date("Y-m-d H:i",strtotime($tanggal_masuk.$jam_sekarang));
    $tanggal_selesai=nextDay();
    $keterangan="-";
    $status_bayar=0;
    $status_pengambilan=0;
    $total_biaya = totalBiaya();
    mysqli_query($kon, "INSERT INTO  transaksi  
    VALUES (
        '',
        '$no_invoice',
        '$tanggal_transaksi',
        '$id_pengguna',
        '$id_pelanggan',
        '$nama_pelanggan',
        '$no_telp',
        '$alamat',
        '$id_layanan',
        '$id_jenis_layanan',
        '$berat',
        '$tanggal_masuk',
        '$tanggal_selesai',
        '$total_biaya',
        '$status_bayar',
        '$status_pengambilan',
        '$keterangan'
    )");
      header("Location:../transaksi");
 }
 $hasil=mysqli_query($kon,"select * from profil_aplikasi order by nama_aplikasi desc limit 1");
 $data = mysqli_fetch_array($hasil); 
    $query = mysqli_query($kon, "SELECT max(id_transaksi) as kodeTerbesar FROM transaksi");
    $row = mysqli_fetch_array($query);
    $id_transaksi = $row['kodeTerbesar'];
    $id_transaksi++;
    $huruf = "T";
    $kodetransaksi = $huruf . sprintf("%04s", $id_transaksi);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Aplikasi Selamanik Laundry</title>
    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="assets/font/font.css" rel="stylesheet">
    <link href="../../../assets/css/user-main.css" rel="stylesheet">
    <link href="../../../assets/css/beranda.css" rel="stylesheet">
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://cdn.tailwindcss.com?plugins=forms,typography,aspect-ratio,line-clamp"></script>

</head>
<script>
tailwind.config = {
    theme: {
        extend: {
            colors: {
                clifford: '#da373d',
                text: "#13271d",
                textsecond: "#fdb535",
            }
        }
    }
}
</script>
<style type="text/tailwindcss">
    @layer utilities {
      .content-auto {
        content-visibility: auto;
      }
    }
  </style>

<body class="overflow-x-hidden">
    <ul class="md:flex hidden px-4 py-2 fixed w-[100%] z-[99]  bg-blue-800">
        <div class="logo">
            <img src="../../aplikasi/logo/logo.png" class="imgs ">
            <span class="text-white"><?php echo ucfirst($data['nama_aplikasi']);?></span>
        </div>
        <div style="display: flex;">
            <li>
                <a class="active" href="../">
                    <div class="navbar-icon">
                        <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />
                            </svg>
                        </div>
                        <!-- <span>Beranda</span> -->
                    </div>
                </a>
            </li>
            <li>
                <a href="../order/">
                    <div class="navbar-icon border-b-2 pb-1">
                        <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z" />
                            </svg>
                        </div>
                        <!-- <span>Order</span> -->
                    </div>
                </a>
            </li>
            <li>
                <a href="../transaksi">
                    <div class="navbar-icon">
                        <div class="icons ">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
                            </svg>
                        </div>
                        <!-- <span>Transaksi</span> -->
                    </div>
                </a>
            </li>
            <li style="float:right">
                <a href="../profil/">
                    <div class="navbar-icon">
                        <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
                            </svg>
                        </div>
                        <!-- <span>Profil</span> -->
                    </div>
                </a>
            </li>
        </div>
    </ul>
    <div class=" flex w-[100vw]   min-h-[100vh] mb-10 md:mb-0">
        <div class="p-10 w-[100%] flex pt-[6rem]">
            <form method="POST"
                class="flex w-[100%] justify-center flex-wrap md:flex-nowrap md:px-10 md:py-5 rounded md:border border-gray-700">
                <div class="flex  flex-col rounded-lg shadow-lg border border-gray-400">
                    <div class="bg-blue-600 w-[20rem] rounded-t-lg px-4 py-2">
                        <p class="text-white font-bold ">Data Order</p>
                    </div>
                    <div class="p-3">
                        <input type="hidden" value="<?= $kodetransaksi; ?>" name="no_invoice">
                        <div class="mb-4">
                            <label class="block text-gray-500 text-sm font-semibold mb-2" for="username">
                                Nama
                            </label>
                            <input
                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                id="username" type="text" placeholder="Nama" name="nama_pelanggan">
                        </div>
                        <div class="mb-4">
                            <label class="block text-gray-500 text-sm font-semibold mb-2" for="username">
                                No Telp
                            </label>
                            <input
                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                id="username" type="text" placeholder="No Telepon" name="no_telp">
                        </div>
                        <div class="mb-4">
                            <label class="block text-gray-500 text-sm font-semibold mb-2" for="username">
                                Alamat
                            </label>
                            <textarea id="message" rows="4"
                                class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500  dark:border-gray-600 dark:placeholder-gray-400  dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Write your thoughts here..." name="alamat"></textarea>

                        </div>
                        <div class="mb-4">
                            <label class="block text-gray-500 text-sm font-semibold mb-2" for="username">
                                Berat
                            </label>
                            <input
                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                id="berat" type="number" placeholder="(kg)" name="berat">
                        </div>
                        <div class="mb-4    ">
                            <div class="relative">
                                <select
                                    class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="pilih_layanan" name="layanan">
                                    <option value="">Pilih Layanan</option>
                                    <?php
                                    $sql = "select * from layanan order by id_layanan asc";
                                    $hasil = mysqli_query($kon, $sql);
                                    while ($data = mysqli_fetch_array($hasil)) :
                                    ?>
                                    <option
                                        value="[<?php echo $data['id_layanan']; ?>,'<?php echo $data['nama_layanan'];?>']">
                                        <?php echo $data['nama_layanan']; ?></option>
                                    <?php endwhile; ?>
                                </select>
                            </div>
                        </div>

                        <div class="mb-4">
                            <div class="relative">
                                <select
                                    class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="jenis_layanan" name="jenis_layanan">

                                </select>
                            </div>
                        </div>
                        <div class="mb-4    ">
                            <div class="relative">
                                <select
                                    class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="kecamatan" name="kecamatan">
                                    <option>Pilih Kecamatan</option>
                                    <option value="2000">Kecamatan Banjarnegara</option>
                                    <option value="4000">Kecamatan Madukara</option>
                                    <option value="6000">Kecamatan Sigaluh</option>
                                    <option value="9000">Kecamatan Wanadadi</option>
                                </select>
                            </div>
                        </div>

                        <!-- <div class="relative">
                            <select
                                class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                id="grid-state">
                                <option>New Mexico</option>
                                <option>Missouri</option>
                                <option>Texas</option>
                            </select>
                        </div> -->
                    </div>
                </div>
                <div
                    class="md:ml-6 md:mt-0 mt-6  flex flex-col md:w-full min-w-[20rem] rounded-lg shadow-lg border border-gray-400">
                    <div class="bg-blue-600  rounded-t-lg px-4 py-2">
                        <p class="text-white font-bold ">Detail Order</p>
                    </div>

                    <div class="flex flex-col  p-4">
                        <div class="flex flex-col">
                            <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                                <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                                    <div class="overflow-hidden">
                                        <table class="min-w-full text-left text-sm font-light">
                                            <thead class="border-b font-medium dark:border-neutral-500">
                                                <tr>
                                                    <th scope="col" class="px-6 py-4">Layanan</th>
                                                    <th scope="col" class="px-6 py-4">Jenis</th>
                                                    <th scope="col" class="px-6 py-4">Berat</th>
                                                    <th scope="col" class="px-6 py-4">Estimasi</th>
                                                    <th class="px-6 py-4">Tanggal Selesai</th>
                                                    <th class=" px-6 py-4">Ongkir</th>
                                                    <th scope="col" class="px-6 py-4">Harga</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="border-b dark:border-neutral-500">
                                                    <td class="whitespace-nowrap px-6 py-4" id="row_layanan">
                                                    </td>
                                                    <td class="whitespace-nowrap px-6 py-4" id="row_jenis_layanan">
                                                    </td>
                                                    <td class="whitespace-nowrap px-6 py-4" id="row_berat">
                                                    </td>
                                                    <td class="whitespace-nowrap px-6 py-4" id="row_estimasi">
                                                    </td>
                                                    <td class="whitespace-nowrap px-6 py-4" id="row_tanggal_selesai">
                                                    </td>
                                                    <td class="whitespace-nowrap px-6 py-4" id="row_ongkir">
                                                    </td>
                                                    <td class="whitespace-nowrap px-6 py-4" id="row_harga">
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="self-end px-4 py-4" id="total">Total : Rp.0</p>
                        <div class="flex justify-end">
                            <button name="pesan" type="submit" class="flex text-center cursor-pointer bg-teal-500 hover:bg-teal-400 text-white font-bold py-2 px-4
                                rounded inline-flex items-center">
                                <span class="  ">Pesan Sekarang</span>
                            </button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
        <div class="temp fixed w-[100vw]">
            <div class="bottom-nav ">
                <a class="tabs" href="../">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />
                        </svg>
                    </div>
                    <span>Beranda</span>
                </a>
                <a class="tabs" href="">
                    <div class="icon border-white border-b-2">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z" />
                        </svg>
                    </div>
                    <span>Order</span>
                </a>
                <a class="tabs" href="../transaksi">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
                        </svg>

                    </div>
                    <span>Transaksi</span>
                </a>
                <a class="tabs" href="../profil">
                    <div class="icon ">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
                        </svg>
                    </div>
                    <span>Profil</span>
                </a>
            </div>
        </div>

    </div>
    <script src="../../../assets/vendor/jquery/jquery.min.js"></script>
    <script src="../../../assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script>
    $('#pilih_layanan').bind('change', function() {
        jenis_layanan();
        // tabel_layanan();
    });
    $('#kecamatan').bind('change', function() {
        var kecamatan = $("#kecamatan").val();
        var jenisLayanan = $("#jenis_layanan").val();
        var beratPakaian = $("#berat").val();
        jenisLayanan = eval(jenisLayanan);
        $("#row_ongkir").html(`Rp.${kecamatan}`);
        $("#row_harga").html(`Rp.${beratPakaian*jenisLayanan[1]}`);
        var total = beratPakaian * jenisLayanan[1] + parseInt(kecamatan);
        $("#total").html(`Total : Rp.${total}`);


    });
    $('#berat').change(function() {

        var beratPakaian = $("#berat").val();
        $("#row_berat").html(`${beratPakaian} (kg)`);
    });
    $('#jenis_layanan').bind('change', function() {
        var jenisLayanan = $("#jenis_layanan").val()
        jenisLayanan = eval(jenisLayanan)
        $("#row_jenis_layanan").html(jenisLayanan[2])
        var currentDate = new Date();
        let dateEnd = currentDate.setDate(currentDate.getDate() + jenisLayanan[3]);
        $("#row_estimasi").html(jenisLayanan[3]);
        $("#row_tanggal_selesai").html(new Date(dateEnd).toLocaleDateString());

        // // var selectedText = window.getSelection().toString();
        // console.log(layanan.val());
    });

    function jenis_layanan() {
        var layanan = $("#pilih_layanan").val();
        layanan = eval(layanan);
        console.log(layanan[0]);
        $.ajax({
            url: '../../transaksi/ambil-data-jenis-layanan.php',
            method: "POST",
            data: {
                layanan: layanan[0]
            },
            async: false,
            dataType: 'json',
            success: function(data) {
                var row_layanan = $("#row_layanan");
                row_layanan.html(layanan[1])
                var html = ' <option value="">Pilih Jenis Layanan</option>';
                var i;
                for (i = 0; i < data.length; i++) {
                    console.log(data[i].tarif);
                    html +=
                        `<option value="[${data[i].id_jenis_layanan},${data[i].tarif},'${data[i].nama_jenis_layanan}',${data[i].estimasi_waktu}]">${data[i].nama_jenis_layanan}</option>`;
                }
                $('#jenis_layanan').html(html);

            }
        })
    }

    function tabel_layanan() {
        var layanan = $("#pilih_layanan").val();
        var jenis_layanan = $("#jenis_layanan").val();

        $.ajax({
            method: 'POST',
            dataType: 'html',
            url: '../transaksi/detail-jenis-layanan.php',
            data: {
                jenis_layanan: jenis_layanan,
                layanan: layanan
            },
            success: function(data) {
                $('#detail_jenis_layanan').html(data);
                hitung_total_biaya();
            }
        });
    }
    </script>
</body>

</html>