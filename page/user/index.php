<?php
 session_start();
 if  (!isset($_SESSION["id_pelanggan"])){
   header("Location:../../loginuser.php");
 }
 include '../../config/database.php';
 $hasil=mysqli_query($kon,"select * from profil_aplikasi order by nama_aplikasi desc limit 1");
 $data = mysqli_fetch_array($hasil); 
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Aplikasi Selamanik Laundry</title>
    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="assets/font/font.css" rel="stylesheet">
    <link href="../../assets/css/user-main.css" rel="stylesheet">
    <link href="../../assets/css/beranda.css" rel="stylesheet">
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://cdn.tailwindcss.com?plugins=forms,typography,aspect-ratio,line-clamp"></script>

</head>
<script>
tailwind.config = {
    theme: {
        extend: {
            colors: {
                clifford: '#da373d',
                text: "#13271d",
                textsecond: "#fdb535",
            }
        }
    }
}
</script>
<style type="text/tailwindcss">
    @layer utilities {
      .content-auto {
        content-visibility: auto;
      }
    }
  </style>

<body>
    <ul class="md:flex hidden px-4 py-2 fixed w-[100%] z-[99]  bg-blue-800">
        <div class="logo">
            <img src="../aplikasi/logo/logo.png" class="imgs ">
            <span class="text-white"><?php echo ucfirst($data['nama_aplikasi']);?></span>
        </div>
        <div style="display: flex;">
            <li>
                <a class="active" href="">
                    <div class="navbar-icon border-b-2 pb-1">
                        <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />
                            </svg>
                        </div>
                        <!-- <span>Beranda</span> -->
                    </div>
                </a>
            </li>
            <li>
                <a href="order">
                    <div class="navbar-icon">
                        <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z" />
                            </svg>
                        </div>
                        <!-- <span>Order</span> -->
                    </div>
                </a>
            </li>
            <li>
                <a href="transaksi">
                    <div class="navbar-icon">
                        <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
                            </svg>
                        </div>
                        <!-- <span>Transaksi</span> -->
                    </div>
                </a>
            </li>
            <li style="float:right">
                <a href="profil">
                    <div class="navbar-icon">
                        <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
                            </svg>
                        </div>
                        <!-- <span>Profil</span> -->
                    </div>
                </a>
            </li>
        </div>
    </ul>
    <div class=" flex w-[100%]">
        <div class=" w-[100%]">
            <section class="bg-blue-800 pt-[5rem]">
                <div class="flex py-4 ">
                    <div class="flex flex-col md:text-6xl text-2xl justify-center md:mr-10 text-white">
                        <h1 class=" font-light">We Are The Best</h1>
                        <h1 class=" font-bold md:mb-4 mb-1 md:w-[30rem]">Laundry Sevices</h1>
                        <h1 class="md:text-3xl text-xl md:w-[30rem] md:mb-8">Kami melayani jasa laundry dengan layanan
                            aplikasi
                            online
                        </h1>
                        <div class="flex ">
                            <a href="order" class="cursor-pointer mr-2 bg-teal-300 hover:bg-teal-400 text-gray-800 font-bold py-2 px-4
                                rounded inline-flex items-center">
                                <div class="w-[32px]">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                        <path strokeLinecap="round" strokeLinejoin="round"
                                            d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z" />
                                    </svg>
                                </div>
                                <span class="ml-2 text-base">Pesan Sekarang</span>
                            </a>

                        </div>
                    </div>
                    <div>
                        <img src="../../assets/img/laundry.png" alt="" class="md:h-[30rem] h-[0px] hidden md:flex">
                    </div>
                </div>
            </section>
            <section>
                <h1>Our Product</h1>
                <div class="container1">
                    <div class="card">
                        <div class="card__header">
                            <img src="https://t-2.tstatic.net/jogja/foto/bank/images/dry-cleaning-atau-cuci-kering_20180718_114039.jpg"
                                alt="card__image" class="card__image" width="600">
                        </div>
                        <div class="card__body">
                            <span class="tag tag-blue">Best Seller</span>
                            <h4>Cuci Kering</h4>
                            <p>Proses mencuci pakaian tanpa menggunakan air melainkan menggunakan cairan kimia tertentu.
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card__header">
                            <img src="https://www.sakulaundry.com/wp-content/uploads/2022/08/setrika34-1080x675.jpg"
                                alt="card__image" class="card__image" width="600">
                        </div>
                        <div class="card__body">
                            <span class="tag tag-blue">Best Seller</span>
                            <h4>Cuci Komplit</h4>
                            <p>mencuci pakaian berupa baju, celana atau yang lainnya. Pelayanan ini
                                bisa dikatakan lebih murah bila memang barang yang ingin dikerjakan tidak banyak dan
                                beragam.
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card__header">
                            <img src="https://images.ctfassets.net/ajjw8wywicb3/2sGz5dCo8LlJoenyCauJ9H/50da004ddfeb3aa2fa839d854d3c9c67/HOW_TO_WASH_CLOTES_How_to_do_Laundry_570x310.jpg?fm=png"
                                alt="card__image" class="card__image" width="600">
                        </div>
                        <div class="card__body">
                            <span class="tag tag-blue">Best Seller</span>
                            <h4>Cuci Satuan</h4>
                            <p>mencuci pakaian berupa baju, celana atau yang lainnya dengan jumlah satuan. Pelayanan ini
                                bisa dikatakan lebih murah bila memang barang yang ingin dikerjakan tidak banyak dan
                                beragam.


                            </p>
                        </div>
                    </div>

                </div>
            </section>
        </div>
        <div class="temp fixed w-[100vw]">
            <div class="bottom-nav ">
                <a class="tabs " href="">
                    <div class="icon border-white border-b-2">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />
                        </svg>
                    </div>
                    <span>Beranda</span>
                </a>
                <a class="tabs" href="order">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z" />
                        </svg>
                    </div>
                    <span>Order</span>
                </a>
                <a class="tabs" href="transaksi">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
                        </svg>
                    </div>
                    <span>Transaksi</span>
                </a>
                <a class="tabs" href="profil">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
                        </svg>
                    </div>
                    <span>Profil</span>
                </a>
            </div>
        </div>

    </div>
</body>

</html>