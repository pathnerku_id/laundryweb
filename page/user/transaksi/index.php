<?php
 session_start();
 if  (!isset($_SESSION["id_pelanggan"])){
   header("Location:../../../loginuser.php");
 }
 include '../../../config/database.php';
 $hasil=mysqli_query($kon,"select * from profil_aplikasi order by nama_aplikasi desc limit 1");
 $data = mysqli_fetch_array($hasil); 
 $id_pelanggan = $_SESSION["id_pelanggan"];
 $result=mysqli_query($kon,"SELECT * FROM `transaksi` WHERE transaksi.id_pelanggan = '$id_pelanggan'");

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Aplikasi Selamanik Laundry</title>
    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="assets/font/font.css" rel="stylesheet">
    <link href="../../../assets/css/user-main.css" rel="stylesheet">
    <link href="../../../assets/css/beranda.css" rel="stylesheet">
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://cdn.tailwindcss.com?plugins=forms,typography,aspect-ratio,line-clamp"></script>
</head>
<script>
tailwind.config = {
    theme: {
        extend: {
            colors: {
                clifford: '#da373d',
                text: "#13271d",
                textsecond: "#fdb535",
            }
        }
    }
}
</script>
<style type="text/tailwindcss">
    @layer utilities {
      .content-auto {
        content-visibility: auto;
      }
    }
  </style>

<body class="overflow-x-hidden">
    <ul class="md:flex hidden px-4 py-2 fixed w-[100%] z-[99]  bg-blue-800">
        <div class="logo">
            <img src="../../aplikasi/logo/logo.png" class="imgs ">
            <span class="text-white"><?php echo ucfirst($data['nama_aplikasi']);?></span>
        </div>
        <div style="display: flex;">
            <li>
                <a class="active" href="../">
                    <div class="navbar-icon">
                        <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />
                            </svg>
                        </div>
                        <!-- <span>Beranda</span> -->
                    </div>
                </a>
            </li>
            <li>
                <a href="../order">
                    <div class="navbar-icon ">
                        <div class="icons ">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z" />
                            </svg>
                        </div>
                        <!-- <span>Order</span> -->
                    </div>
                </a>
            </li>
            <li>
                <a href="">
                    <div class="navbar-icon border-b-2 pb-1">
                        <div class="icons ">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
                            </svg>
                        </div>
                        <!-- <span>Transaksi</span> -->
                    </div>
                </a>
            </li>
            <li style="float:right">
                <a href="../profil/">
                    <div class="navbar-icon">
                        <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
                            </svg>
                        </div>
                        <!-- <span>Profil</span> -->
                    </div>
                </a>
            </li>
        </div>
    </ul>
    <div class=" flex w-[100vw]   min-h-[100vh] mb-10 md:mb-0">
        <div class="p-10 w-[100%] flex-col flex pt-[6rem]">

            <form>
                <div class="relative">
                    <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                        <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="none"
                            stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                        </svg>
                    </div>
                    <input type="search" id="default-search"
                        class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        placeholder="Search" required>
                    <button type="submit"
                        class="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Search</button>
                </div>
            </form>
            <div class="border overflow-x-auto mt-10 border-gray-500 rounded  shadow-lg">
                <table class=" min-w-full text-left text-sm font-light">
                    <thead class="border-b font-medium dark:border-neutral-500">
                        <tr>
                            <th scope="col" class="px-6 py-4">No</th>
                            <th scope="col" class="px-6 py-4">Tanggal</th>
                            <th scope="col" class="px-6 py-4">No Invoice</th>
                            <th scope="col" class="px-6 py-4">Nama</th>
                            <!-- <th scope="col" class="px-6 py-4">Layanan</th> -->
                            <th scope="col" class="px-6 py-4">Total Biaya</th>
                            <th scope="col" class="px-6 py-4">Pembayaran</th>
                            <th scope="col" class="px-6 py-4">Pengambilan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no=0;
                        //Menampilkan data dengan perulangan while
                        while ($row = mysqli_fetch_array($result)):
                        $no++;
                        ?>
                        <tr class="border-b dark:border-neutral-500">
                            <td class="whitespace-nowrap px-6 py-4"><?=$no?></td>
                            <td class="whitespace-nowrap px-6 py-4"><?=$row["tanggal_transaksi"]?></td>
                            <td class="whitespace-nowrap px-6 py-4"><?=$row["no_invoice"]?></td>
                            <td class="whitespace-nowrap px-6 py-4"><?=$row["nama_pelanggan"]?></td>
                            <td class="whitespace-nowrap px-6 py-4">Rp. <?=$row["total_biaya"]?></td>
                            <td class="whitespace-nowrap px-6 py-4">
                                <?php if ($row["status_bayar"] == 0): ?>
                                <span
                                    class="bg-yellow-100 text-yellow-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded ">Belum
                                    Bayar
                                </span>
                                <?php endif; ?>
                                <?php if ($row["status_bayar"] == 1): ?>
                                <span
                                    class="bg-teal-100 text-teal-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded ">Sudah
                                    Bayar
                                </span>
                                <?php endif; ?>
                            </td>
                            <td class="whitespace-nowrap px-6 py-4">
                                <?php if ($row["status_pengambilan"] == 0): ?>
                                <span
                                    class="bg-yellow-100 text-yellow-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded ">Belum
                                    Diambil
                                </span>
                                <?php endif; ?>
                                <?php if ($row["status_pengambilan"] == 1): ?>
                                <span
                                    class="bg-teal-100 text-teal-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded ">Sudah
                                    Diambil
                                </span>
                                <?php endif; ?>
                            </td>

                        </tr>

                        <?php endwhile; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="temp fixed w-[100vw]">
            <div class="bottom-nav ">
                <a class="tabs" href="../">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />
                        </svg>
                    </div>
                    <span>Beranda</span>
                </a>
                <a class="tabs" href="../order">
                    <div class="icon ">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z" />
                        </svg>
                    </div>
                    <span>Order</span>
                </a>
                <a class="tabs" href="">
                    <div class="icon border-white border-b-2">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
                        </svg>

                    </div>
                    <span>Transaksi</span>
                </a>
                <a class="tabs" href="../profil">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
                        </svg>
                    </div>
                    <span>Profil</span>
                </a>
            </div>
        </div>

    </div>
    <script>
    </script>
</body>

</html>