<?php
 session_start();
 if  (!isset($_SESSION["id_pelanggan"])){
   header("Location:../../../loginuser.php");
 }
 include '../../../config/database.php';
 $hasil=mysqli_query($kon,"select * from profil_aplikasi order by nama_aplikasi desc limit 1");
 $data = mysqli_fetch_array($hasil); 
 $id_pelanggan = $_SESSION["id_pelanggan"];
 $result=mysqli_query($kon,"SELECT * FROM `pelanggan` WHERE pelanggan.id_pelanggan = '$id_pelanggan'");
 $row = mysqli_fetch_array($result);
 if (isset($_POST["simpan"])) {
    include '../../../config/database.php';
    function input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    $kode_pelanggan=input($row["kode_pelanggan"]);
    $nama_pelanggan=input($_POST["nama_pelanggan"]);
    $username=input($_POST["username"]);
    $password=input($_POST["password"]);
    $no_telp=input($_POST["no_telp"]);
    $alamat=input($_POST["alamat_pelanggan"]);
    $jk=input($_POST["jenis_kelamin"]);
    $tanggal_lahir=input($_POST["tanggal_lahir"]);
    $status=input($row["status"]);
    $sql="update pelanggan set
    nama_pelanggan='$nama_pelanggan',
    username='$username',
    password='$password',
    no_telp='$no_telp',
    alamat_pelanggan='$alamat',
    jenis_kelamin='$jk',
    tanggal_lahir='$tanggal_lahir',
    status='$status'
    where id_pelanggan=$id_pelanggan";
    header("Location:../profil");
    //Mengeksekusi query 
    $update_pelanggan=mysqli_query($kon,$sql);
    
      //Mengeksekusi query 
    //   $simpan_pelanggan=mysqli_query($kon,"UPDATE pelanggan 
    //   SET 
    //   kode_pelanggan='$kode_pelanggan',
    //   nama_pelanggan='$nama_pelanggan',
    //   username='$username',
    //   password='$password',
    //   no_telp='$no_telp',
    //   alamat_pelanggan='$alamat',
    //   jenis_kelamin'$jk',
    //   tanggal_lahir='$tanggal_lahir',
    //   status='$status'
    //   WHERE id_pelanggan = $id_pelanggan");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Aplikasi Selamanik Laundry</title>
    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="assets/font/font.css" rel="stylesheet">
    <link href="../../../assets/css/user-main.css" rel="stylesheet">
    <link href="../../../assets/css/beranda.css" rel="stylesheet">
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://cdn.tailwindcss.com?plugins=forms,typography,aspect-ratio,line-clamp"></script>

</head>
<script>
tailwind.config = {
    theme: {
        extend: {
            colors: {
                clifford: '#da373d',
                text: "#13271d",
                textsecond: "#fdb535",
            }
        }
    }
}
</script>
<style type="text/tailwindcss">
    @layer utilities {
      .content-auto {
        content-visibility: auto;
      }
    }
  </style>

<body class="overflow-x-hidden">
    <ul class="md:flex hidden px-4 py-2 fixed w-[100%] z-[99]  bg-blue-800">
        <div class="logo">
            <img src="../../aplikasi/logo/logo.png" class="imgs ">
            <span class="text-white"><?php echo ucfirst($data['nama_aplikasi']);?></span>
        </div>
        <div style="display: flex;">
            <li>
                <a class="active" href="../">
                    <div class="navbar-icon">
                        <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />
                            </svg>
                        </div>
                        <!-- <span>Beranda</span> -->
                    </div>
                </a>
            </li>
            <li>
                <a href="../order">
                    <div class="navbar-icon ">
                        <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z" />
                            </svg>
                        </div>
                        <!-- <span>Order</span> -->
                    </div>
                </a>
            </li>
            <li>
                <a href="../transaksi">
                    <div class="navbar-icon">
                        <div class="icons ">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
                            </svg>
                        </div>
                        <!-- <span>Transaksi</span> -->
                    </div>
                </a>
            </li>
            <li style="float:right">
                <a href="#about">
                    <div class="navbar-icon border-b-2 pb-1">
                        <div class="icons">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                                stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round"
                                    d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
                            </svg>
                        </div>
                        <!-- <span>Profil</span> -->
                    </div>
                </a>
            </li>
        </div>
    </ul>
    <div class=" flex w-[100vw]   min-h-[100vh] mb-10 md:mb-0">
        <div class="p-10 w-[100%] flex pt-[6rem]">
            <div
                class="flex w-[100%] justify-center flex-wrap md:flex-nowrap md:px-10 md:py-5 rounded md:border border-gray-700">
                <div class="flex  flex-col rounded-lg shadow-lg border border-gray-400">
                    <div class="bg-blue-600 w-[20rem] rounded-t-lg px-4 py-2">
                        <p class="text-white font-bold ">Profil</p>
                    </div>
                    <form class="p-3" method="POST">
                        <div class="mb-4">
                            <label class="block text-gray-500 text-sm font-semibold mb-2" for="username">
                                Nama
                            </label>
                            <input
                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                id="username" type="text" placeholder="Nama" name="nama_pelanggan"
                                value="<?=$row["nama_pelanggan"]?>">
                        </div>
                        <div class="mb-4">
                            <label class="block text-gray-500 text-sm font-semibold mb-2" for="username">
                                Username
                            </label>
                            <input
                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                id="username" type="text" placeholder="Username" name="username"
                                value="<?=$row["username"]?>">
                        </div>
                        <div class="mb-4">
                            <label class="block text-gray-500 text-sm font-semibold mb-2" for="username">
                                Password
                            </label>
                            <input
                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                id="username" type="text" placeholder="Password" name="password"
                                value="<?=$row["password"]?>">
                        </div>
                        <div class="mb-4">
                            <label class="block text-gray-500 text-sm font-semibold mb-2" for="username">
                                Alamat
                            </label>
                            <textarea id="message" rows="4" name="alamat_pelanggan"
                                class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500  dark:border-gray-600 dark:placeholder-gray-400  dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Write your thoughts here..." value=""><?=$row["alamat_pelanggan"]?>
                                </textarea>

                        </div>
                        <div class="mb-4">
                            <label class="block text-gray-500 text-sm font-semibold mb-2" for="username">
                                No Telp
                            </label>
                            <input
                                class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                id="username" type="text" placeholder="No Telepon" name="no_telp"
                                value="<?=$row["no_telp"]?>">
                        </div>
                        <div class="mb-4">
                            <label class="block text-gray-500 text-sm font-semibold mb-2" for="username">
                                Tanggal Lahir
                            </label>
                            <input type="date" name="tanggal_lahir" class="form-field animation a4"
                                value="<?=$row["tanggal_lahir"]?>">
                        </div>

                        <div class="mb-4    ">
                            <div class="relative">
                                <label class="block text-gray-500 text-sm font-semibold mb-2" for="username">
                                    Jenis Kelamin
                                </label>
                                <select
                                    class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="grid-state" name="jenis_kelamin">
                                    <option value="2" <?= $row["jenis_kelamin"]==="2"?"selected":"" ?>>Perempuan
                                    </option>
                                    <option value="1" <?= $row["jenis_kelamin"]==="1"?"selected":"" ?>>Laki-laki
                                    </option>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="relative">
                            <select
                                class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                id="grid-state">
                                <option>New Mexico</option>
                                <option>Missouri</option>
                                <option>Texas</option>
                            </select>
                        </div> -->
                        <div class="flex justify-evenly">
                            <button class="flex text-center cursor-pointer bg-teal-500 hover:bg-teal-400 text-white font-bold py-2 px-4
                                rounded inline-flex items-center" type="submit" name="simpan">
                                <span class="  ">Simpan</span>
                            </button>
                            <a href="logout.php" class="cursor-pointer flex text-center cursor-pointer bg-red-500 hover:bg-teal-400 text-white font-bold py-2 px-4
                                rounded inline-flex items-center">
                                <span class="  ">Logout</span>
                            </a>
                        </div>
                    </form>

                </div>

            </div>
        </div>
        <div class="temp fixed w-[100vw]">
            <div class="bottom-nav ">
                <a class="tabs" href="../">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25" />
                        </svg>
                    </div>
                    <span>Beranda</span>
                </a>
                <a class="tabs" href="../order">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z" />
                        </svg>
                    </div>
                    <span>Order</span>
                </a>
                <a class="tabs" href="../transaksi">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
                        </svg>

                    </div>
                    <span>Transaksi</span>
                </a>
                <a class="tabs" href="">
                    <div class="icon border-white border-b-2">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                            stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round"
                                d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
                        </svg>

                    </div>
                    <span>Profil</span>
                </a>
            </div>
        </div>

    </div>
</body>

</html>