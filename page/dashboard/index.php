<script>
    //Membuat judul
    $('title').text('Dashboard');
</script>
<!-- Import chart js -->
<script src="assets/js/chart/chart.js"></script>

<div class="container-fluid" >
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
   </div>
    <div class="row">
    <?php
        //Menampilkan total transaksi
        include 'config/database.php';
        $hari_ini=date('Y-m-d');
        $id_pengguna=0;
        //Jika level user adalah admin maka hanya menampilkan transaksi yang dilakukan admin tersebut
        if ($_SESSION["level"]=="Admin"){
            $id_pengguna=$_SESSION["id_pengguna"];
            $sql="select SUM(total_biaya) as hari_ini from transaksi where date(tanggal_transaksi)='$hari_ini' and id_pengguna=$id_pengguna";
         }else {
            $sql="select SUM(total_biaya) as hari_ini from transaksi where date(tanggal_transaksi)='$hari_ini'";
        }

        $hasil=mysqli_query($kon,$sql);
        $data = mysqli_fetch_array($hasil);       
    ?>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">transaksi hari ini</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">Rp. <?php  echo number_format($data['hari_ini'],0,',','.');?></div>
            </div>
            <div class="col-auto">
            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
            </div>
            </div>
        </div>
        </div>
    </div>
    <?php 
        include 'config/database.php';
        $bulan_ini=date('m');
        $tahun_ini=date('Y');
        $id_pengguna=0;
         //Jika level user adalah admin maka hanya menampilkan transaksi yang dilakukan admin tersebut
        if ($_SESSION["level"]=="Admin"){
            $id_pengguna=$_SESSION["id_pengguna"];
            $sql="select SUM(total_biaya) as bulan_ini from transaksi where MONTH(tanggal_transaksi)='$bulan_ini' and YEAR(tanggal_transaksi)='$tahun_ini' and id_pengguna=$id_pengguna";
        }else {
        $sql="select SUM(total_biaya) as bulan_ini from transaksi where MONTH(tanggal_transaksi)='$bulan_ini' and YEAR(tanggal_transaksi)='$tahun_ini'";
        }
        $hasil=mysqli_query($kon,$sql);
        $data = mysqli_fetch_array($hasil);
    ?>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">transaksi bulan ini</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">Rp. <?php  echo number_format($data['bulan_ini'],0,',','.');?></div>
            </div>
            <div class="col-auto">
                <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
            </div>
            </div>
        </div>
        </div>
    </div>
    <?php 
        include 'config/database.php';
        $tahun_ini=date('Y');
         //Jika level user adalah admin maka hanya menampilkan transaksi yang dilakukan admin tersebut
        if ($_SESSION["level"]=="Admin"){
            $id_pengguna=$_SESSION["id_pengguna"];
            $sql="select SUM(total_biaya) as tahun_ini from transaksi where year(tanggal_transaksi)='$tahun_ini' and id_pengguna=$id_pengguna";
        }else{
            $sql="select SUM(total_biaya) as tahun_ini from transaksi where year(tanggal_transaksi)='$tahun_ini'";
        }

        $hasil=mysqli_query($kon,$sql);
        $data = mysqli_fetch_array($hasil);
    ?>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">transaksi tahun ini</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">Rp. <?php  echo number_format($data['tahun_ini'],0,',','.');?></div>
            </div>
            <div class="col-auto">
                <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
            </div>
            </div>
        </div>
        </div>
    </div>
    <?php 
        include 'config/database.php';
        //Jika level user adalah admin maka hanya menampilkan transaksi yang dilakukan admin tersebut
        if ($_SESSION["level"]=="Admin"){
            $id_pengguna=$_SESSION["id_pengguna"];
            $sql="select SUM(total_biaya) as selama_ini from transaksi t  where id_pengguna=$id_pengguna";
        }else {
            $sql="select SUM(total_biaya) as selama_ini from transaksi";
        }

        $hasil=mysqli_query($kon,$sql);
        $data = mysqli_fetch_array($hasil);
    ?>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">transaksi selama ini</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">Rp. <?php  echo number_format($data['selama_ini'],0,',','.');?></div>
            </div>
            <div class="col-auto">
            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>

    <div class="row">
    <div class="col-xl-8 col-lg-7">
        <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary" id="judul_grafik" >Pilih Grafik transaksi</h6>
            <div class="dropdown no-arrow">
            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                <div class="dropdown-header">Dropdown Header:</div>
                <a class="dropdown-item" id="transaksi_bulan_ini" href="#">Transaksi bulan ini</a>
                <a class="dropdown-item" id="transaksi_tahun_ini" href="#">Transaksi tahun ini</a>
                <a class="dropdown-item" id="semua_transaksi" href="#">Semua Transaksi</a>
            </div>
            </div>
        </div>
        <div class="card-body">
            <div id="tampil_grafik">
                <!-- Grafik pendapatan transaksi akan di tampilkan disini menggunakan ajax -->
            </div>
        </div>
        </div>
    </div>

    <div class="col-xl-4 col-lg-5">
        <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Jumlah Transaksi</h6>
            <div class="dropdown no-arrow">
            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                <div class="dropdown-header">Dropdown Header:</div>
                <a class="dropdown-item" href="#" id="transaksi_by_layanan">Transaksi Berdasarkan Layanan</a>
                <a class="dropdown-item" href="#" id="transaksi_by_jenis_layanan">Transaksi Berdasarkan Jenis Layanan</a>
                <a class="dropdown-item" href="#" id="transaksi_by_status_bayar">Transaksi Berdasarkan Status Pembayaran</a>
                <a class="dropdown-item" href="#" id="transaksi_by_status_ambil">Transaksi Berdasarkan Status Pengambilan</a>
            </div>
            </div>
        </div>
        <div class="card-body">
            <div class="chart-pie pt-4" id="tampil_pie_chart">
                <canvas id="pie_chart"></canvas>
           </div>
            <div class="mt-4 text-center small">
            <span class="mr-2">
              <span id="keterangan_chart"></span>
            </span>
            </div>
        </div>
        </div>
    </div>
    </div>
</div>

<script>
    //Saat halaman diload pertama kali fungsi ini yang akan dipanggil
    $(document).ready(function(){
        transaksi_bulan_ini();
        transaksi_by_layanan();
    });

    //Saat user memilih transaksi bulan ini maka fungsi transaksi_bulan_ini() akan dipanggil
    $('#transaksi_bulan_ini').on('click',function(){    
        transaksi_bulan_ini();
    });

    function transaksi_bulan_ini(){
        $.ajax({
            url: 'page/dashboard/transaksi-harian.php',
            method: 'POST',
            success:function(data){
                $('#tampil_grafik').html(data);
                document.getElementById("judul_grafik").innerHTML="Grafik transaksi bulan ini";
            }
        }); 
    }

    //Saat user memilih transaksi tahun ini maka fungsi transaksi_tahun_ini() akan dipanggil
    $('#transaksi_tahun_ini').on('click',function(){    
        $.ajax({
            url: 'page/dashboard/transaksi-tahunan.php',
            method: 'POST',
            success:function(data){
                $('#tampil_grafik').html(data);
                document.getElementById("judul_grafik").innerHTML="Grafik transaksi tahun ini";
            }
        }); 
    });

    //Saat user memilih semua transaksi maka fungsi semua_transaksi() akan dipanggil
    $('#semua_transaksi').on('click',function(){    
        $.ajax({
            url: 'page/dashboard/semua-transaksi.php',
            method: 'POST',
            success:function(data){
                $('#tampil_grafik').html(data);
                document.getElementById("judul_grafik").innerHTML="Grafik transaksi selama ini";
            }
        }); 
    });

    //Saat user memilih transaksi berdasarkan layanan maka fungsi transaksi_by_layanan() akan dipanggil
    $('#transaksi_by_layanan').on('click',function(){    
        transaksi_by_layanan()
    });

    function transaksi_by_layanan(){
        $.ajax({
            url: 'page/dashboard/transaksi-by-layanan.php',
            method: 'POST',
            success:function(data){
                $('#tampil_pie_chart').html(data);
                document.getElementById("keterangan_chart").innerHTML="Jumlah Transaksi Berdasarkan layanan"; 
            }
        }); 
    }

    //Saat user memilih transaksi berdasarkan layanan
    $('#transaksi_by_jenis_layanan').on('click',function(){    
        $.ajax({
            url: 'page/dashboard/transaksi-by-jenis-layanan.php',
            method: 'POST',
            success:function(data){
                $('#tampil_pie_chart').html(data);
                document.getElementById("keterangan_chart").innerHTML="Jumlah Transaksi Berdasarkan Jenis layanan";
            }
        }); 
    });

    //Saat user memilih transaksi berdasarkan status pembayaran
    $('#transaksi_by_status_bayar').on('click',function(){    
        $.ajax({
            url: 'page/dashboard/transaksi-by-status-bayar.php',
            method: 'POST',
            success:function(data){
                $('#tampil_pie_chart').html(data);
                document.getElementById("keterangan_chart").innerHTML="Jumlah Transaksi Berdasarkan Status Pembayaran";
            }
        }); 
    });

    //Saat user memilih transaksi berdasarkan status pengambilan
    $('#transaksi_by_status_ambil').on('click',function(){    
        $.ajax({
            url: 'page/dashboard/transaksi-by-status-ambil.php',
            method: 'POST',
            success:function(data){
                $('#tampil_pie_chart').html(data);
                document.getElementById("keterangan_chart").innerHTML="Jumlah Transaksi Berdasarkan Status Pengambilan";
            }
        }); 
    });

</script>
