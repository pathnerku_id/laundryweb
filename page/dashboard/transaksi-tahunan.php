<?php
    session_start();
    include '../../config/database.php';

    $label = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
    $tahun_ini=date('Y');

    for($bulan = 1;$bulan <= 12;$bulan++)
    {
        if ($_SESSION["level"]=="Admin"){
            $id_pengguna=$_SESSION["id_pengguna"];

            $sql="select SUM(total_biaya) as total
            from transaksi 
            where MONTH(tanggal_transaksi)='$bulan' and YEAR(tanggal_transaksi)='$tahun_ini' and id_pengguna=$id_pengguna";
        }else {
            $sql="select SUM(total_biaya) as total
            from transaksi 
            where MONTH(tanggal_transaksi)='$bulan' and YEAR(tanggal_transaksi)='$tahun_ini'";
        }

        $hasil=mysqli_query($kon,$sql);
        $data=mysqli_fetch_array($hasil);
        $total[] = $data['total'];
    }
?>
<canvas id="grafik_transaksi"></canvas>

<script>
    var ctx = document.getElementById("grafik_transaksi").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: <?php echo json_encode($label); ?>,
            datasets: [{
                label: 'Grafik Transaksi',
                data: <?php echo json_encode($total); ?>,
                backgroundColor: '#2eb8b8',
                borderWidth: 1
            }]
        },
      options: {
            maintainAspectRatio: true,
            layout: {
            padding: {
                left: 10,
                right: 10,
                top: 25,
                bottom: 0
            }
            },
            scales: {
            xAxes: [{
                time: {
                unit: 'month'
                },
                gridLines: {
                display: false,
                drawBorder: false
                },
                ticks: {
                maxTicksLimit: 30
                },
                maxBarThickness: 25,
            }],
    
            },
            legend: {
            display: false
            },
         }
    });
</script>