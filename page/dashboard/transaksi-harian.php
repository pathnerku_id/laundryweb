<?php
    session_start();
    include '../../config/database.php';
    $bulan_ini=date('m');
    $tahun_ini=date('Y');
    //Mendapatkan jumlah hari dalam bulan ini
    $jumHari = cal_days_in_month(CAL_GREGORIAN, $bulan_ini, $tahun_ini);

    for($hari=1;$hari<=$jumHari;$hari++)
    {
        if ($_SESSION["level"]=="Admin"){
            $id_pengguna=$_SESSION["id_pengguna"];
            $sql="select SUM(total_biaya) as total from transaksi where DAY(tanggal_transaksi)='$hari' and MONTH(tanggal_transaksi)='$bulan_ini' and id_pengguna=$id_pengguna";
        }else {
            $sql="select SUM(total_biaya) as total from transaksi where DAY(tanggal_transaksi)='$hari' and MONTH(tanggal_transaksi)='$bulan_ini'";
        }
        
        $hasil=mysqli_query($kon,$sql);
        $data=mysqli_fetch_array($hasil);
        $total[] = $data['total'];
        $label[] = $hari;
    }
?>

<canvas id="grafik_transaksi"></canvas>

<script>
    var ctx = document.getElementById("grafik_transaksi").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: <?php echo json_encode($label); ?>,
            datasets: [{
                label: 'Grafik Transaksi',
                data: <?php echo json_encode($total); ?>,
                backgroundColor: '#2eb8b8',
                borderWidth: 1
            }]
        },
      options: {
            maintainAspectRatio: true,
            layout: {
            padding: {
                left: 10,
                right: 10,
                top: 25,
                bottom: 0
            }
            },
            scales: {
            xAxes: [{
                time: {
                unit: 'month'
                },
                gridLines: {
                display: false,
                drawBorder: false
                },
                ticks: {
                maxTicksLimit: 31
           
                },
                maxBarThickness: 25,
            }],
    
            },
            legend: {
            display: false
            },
         }
    });
</script>