<?php
    session_start();
    include '../../config/database.php';
    if ($_SESSION["level"]=="Admin"){
        $id_pengguna=$_SESSION["id_pengguna"];
        $sql="select year(tanggal_transaksi) as tahun,SUM(total_biaya) as total from transaksi where id_pengguna=$id_pengguna group by tahun";
    }else {
        $sql="select year(tanggal_transaksi) as tahun,SUM(total_biaya) as total from transaksi group by tahun";
    }

    $hasil=mysqli_query($kon,$sql);
    while ($data = mysqli_fetch_array($hasil)) {
    $tahun[] = $data['tahun'];
    $total[] = $data['total'];
    
}
?>
<!-- Grafik di tampilkan di tag canvas -->
<canvas id="grafik_transaksi"></canvas>

<script>
    var ctx = document.getElementById("grafik_transaksi").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: <?php echo json_encode($tahun); ?>,
            datasets: [{
                label: 'Grafik Penjualan',
                data: <?php echo json_encode($total); ?>,
                backgroundColor: '#2eb8b8',
                borderWidth: 1
            }]
        },
      options: {
            maintainAspectRatio: true,
            layout: {
            padding: {
                left: 10,
                right: 10,
                top: 25,
                bottom: 0
            }
            },
            scales: {
            xAxes: [{
                time: {
                unit: 'month'
                },
                gridLines: {
                display: false,
                drawBorder: false
                },
                ticks: {
                maxTicksLimit: 30
                },
                maxBarThickness: 25,
            }],
    
            },
            legend: {
            display: false
            },
         }
    });
</script>