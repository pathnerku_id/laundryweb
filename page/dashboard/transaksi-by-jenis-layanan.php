<?php 
    include '../../config/database.php';
    $sql="select p.nama_jenis_layanan,count(*) as total from transaksi t inner join jenis_layanan p on p.id_jenis_layanan=t.id_jenis_layanan group by p.nama_jenis_layanan asc";
    $hasil=mysqli_query($kon,$sql);
    $no=0;
    $total='';
    $kategori='';
    $nama_jenis_layanan='';

    while ($data = mysqli_fetch_array($hasil)) {
        $nama_jenis_layanan=$data['nama_jenis_layanan'];
        $kategori .= "'$nama_jenis_layanan'". ", ";
        $total .= "".$data['total']."". ", ";
        $bg="RGB(25, 155, 232)";
    }

?>

<canvas id="pie_chart"></canvas>

<script>
    var ctx = document.getElementById("pie_chart");
    var myPieChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: [<?php echo $kategori; ?>],
            datasets: [{
            data: [<?php echo $total; ?>],
            backgroundColor: ['#9900ff', '#0066ff', '#ffff00','#ff0000'],
            hoverBackgroundColor: ['#a31aff', '#3385ff', '#ffff4d', '#ff1a1a'],
            hoverBorderColor: "rgba(234, 236, 244, 1)",
            }],
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            caretPadding: 10,
            },
            legend: {
            display: false
            },
            cutoutPercentage: 80,
        },
    });
</script>
