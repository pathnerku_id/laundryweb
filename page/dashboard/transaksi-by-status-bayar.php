<?php
    include '../../config/database.php';
    $sql="select status_bayar,count(*) as total from transaksi  group by status_bayar asc";
    $hasil=mysqli_query($kon,$sql);

    $no=0;
    $total='';
    $kategori='';
    $status_bayar='';

    while ($data = mysqli_fetch_array($hasil)) {
        if ($data['status_bayar']==0){
            $status_bayar='Belum Bayar';
        }else {
            $status_bayar='Sudah Bayar';
        }
        
        $kategori .= "'$status_bayar'". ", ";
        $total .= "".$data['total']."". ", ";
        $bg="RGB(25, 155, 232)";
    }

  ?>
<canvas id="pie_chart"></canvas>
<script>
  var ctx = document.getElementById("pie_chart");
  var myPieChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
      labels: [<?php echo $kategori; ?>],
      datasets: [{
        data: [<?php echo $total; ?>],
        backgroundColor: ['#ff8000','#009933'],
        hoverBackgroundColor: ['#ff9933', '#00b33c'],
        hoverBorderColor: "rgba(234, 236, 244, 1)",
      }],
    },
    options: {
      maintainAspectRatio: false,
      tooltips: {
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
      },
      legend: {
        display: false
      },
      cutoutPercentage: 80,
    },
  });
</script>