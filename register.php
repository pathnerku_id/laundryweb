<?php

 include 'config/database.php';
 $hasil=mysqli_query($kon,"select * from profil_aplikasi order by nama_aplikasi desc limit 1");
 $data = mysqli_fetch_array($hasil); 
 $query = mysqli_query($kon, "SELECT max(id_pelanggan) as kodeTerbesar FROM pelanggan");
 $data = mysqli_fetch_array($query);
 $id_pelanggan = $data['kodeTerbesar'];
 $id_pelanggan++;
 $huruf = "PN";
 $kodepelanggan = $huruf . sprintf("%04s", $id_pelanggan);
 if (isset($_POST["register"])) {
      $kode_pelanggan=$kodepelanggan;
      $nama_pelanggan=$_POST["nama_pelanggan"];
      $username=$_POST["username"];
      $password=$_POST["password"];
      $no_telp=$_POST["no_telp"];
      $alamat=$_POST["alamat_pelanggan"];
      $jk=$_POST["jk"];
      $tanggal_lahir=$_POST["tanggal_lahir"];
      $status="1";
    // var_dump($row);
    //Query input menginput data kedalam tabel pelanggan
    $sql="insert into pelanggan (kode_pelanggan,nama_pelanggan,username,password,no_telp,alamat_pelanggan,jenis_kelamin,tanggal_lahir,status) values
    ('$kode_pelanggan','$nama_pelanggan','$username','$password','$no_telp','$alamat','$jk','$tanggal_lahir','$status')";

    //Mengeksekusi query 
    $simpan_pelanggan=mysqli_query($kon,$sql);
    if ($simpan_pelanggan ) {
        header("Location:loginuser.php");
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Aplikasi Selamanik Laundry</title>
    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="assets/font/font.css" rel="stylesheet">
    <link href="assets/css/user.css" rel="stylesheet">
</head>
<style>
.test1 {
    display: none;

}

@media only screen and (max-width: 767px) {
    .test {
        display: none;
    }

    .test1 {
        display: flex;
        position: absolute;
        top: 0;
        padding-top: 20px;
    }
}
</style>

<body>


    <div class="container relative">

        <div class="left w-[300px]">
            <!-- <div class="logo">
                <img src="page/aplikasi/logo/logo.png" width="100rem" class="imgs animation a7">
            </div> -->
            <div class="header test1 px-[20px]">
                <h2 class="">Register</h2>
            </div>
            <div class="header test none md:flex">
                <h2 class="absolute">Register</h2>
            </div>
            <form class="form" method="POST">
                <input type="text" name="nama_pelanggan" class="form-field animation a3" placeholder="Masukan Nama">
                <input type="text" name="username" class="form-field animation a3" placeholder="Username">
                <input type="password" name="password" class="form-field animation a4" placeholder="Password">
                <input type="text" name="no_telp" class="form-field animation a4" placeholder="No Telepon">
                <input type="text" name="alamat_pelanggan" class="form-field animation a4" placeholder="Alamat">

                <div class="selectdiv animation a4 form-grup">
                    <label>
                        <select class="" name="jk">
                            <option selected> Jenis Kelamin</option>
                            <option value="1">Laki-Laki</option>
                            <option value="2">Perempuan</option>
                        </select>
                    </label>
                </div>
                <input type="date" name="tanggal_lahir" class="form-field animation a4">

                <p class="animation a5"><a href="loginuser.php">Login</a></p>
                <button class="animation a6" type="submit" name="register">REGISTER</button>
            </form>
        </div>
        <div class="right"></div>
    </div>
    <script></script>
    <!-- Bootstrap core JavaScript-->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="assets/js/sb-admin-2.min.js"></script>

</body>

</html>