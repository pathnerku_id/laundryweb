<?php
    //Memulai session
    session_start();
    //Set session
    $id_pengguna=$_SESSION['id_pengguna'];
    $_SESSION['id_pengguna']='';
    $_SESSION['kode_pengguna']='';
    $_SESSION['nama_pengguna']='';
    $_SESSION['username']='';
    $_SESSION['level']='';
    $_SESSION['foto']='';
   
    //Hapus session
    unset($_SESSION['id_pengguna']);
    unset($_SESSION['kode_pengguna']);
    unset($_SESSION['nama_pengguna']);
    unset($_SESSION['username']);
    unset($_SESSION['level']);
    unset($_SESSION['foto']);

    session_unset();
    session_destroy();

    //Mencatat aktivitas
    $waktu=date("Y-m-d H:i:s");
    $log_aktivitas="Logout";
    include "config/database.php";
    mysqli_query($kon,"insert into log_aktivitas (waktu,aktivitas,id_pengguna) values ('$waktu','$log_aktivitas','$id_pengguna')");

    //Alihakan ke halaman login
    header('Location:login.php');

?>